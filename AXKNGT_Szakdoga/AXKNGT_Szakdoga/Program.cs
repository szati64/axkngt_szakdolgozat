using System;
using System.Configuration;
using System.IO;
using System.Windows.Forms;
using NLog;

namespace AXKNGT_Szakdoga
{
#if WINDOWS || XBOX
    internal static class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly string SavePath = ConfigurationManager.AppSettings["SaveFile"];
        private static readonly string SaveDir = ConfigurationManager.AppSettings["SaveDir"];

        private static void Main()
        {
            try
            {
                var saveDirPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + SaveDir;

                if (!Directory.Exists(saveDirPath))
                {
                    Directory.CreateDirectory(saveDirPath);
                    Logger.Trace("Savedir created to {0}", saveDirPath);
                }

                var fullSavePath = saveDirPath + "\\" + SavePath;
                var saveFileInfo = new FileInfo(fullSavePath);

                if (!saveFileInfo.Exists || !saveFileInfo.IsReadOnly)
                    using (var game = new MyGame(fullSavePath))
                    {
                        game.Run();
                    }
                else
                    MessageBox.Show("You don't have permission to save your in-game progress!", "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                MessageBox.Show("An error has occurred and the application has been terminated!\nRead the log file for details!", "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }
        }
    }
#endif
}