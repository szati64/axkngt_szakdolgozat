﻿using AXKNGT_Szakdoga.Service.Kinect;
using Microsoft.Kinect;
using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;
using NLog;

namespace AXKNGT_Szakdoga.Service.Speech
{
    internal class SpeechProcessor
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public string Recognized { private set; get; }
        public bool IsWorking { get; private set; }

        public bool IsNewRecognized
        {
            get
            {
                if (!_newRecognized)
                    return false;

                _newRecognized = false;
                return true;
            }
        }

        private readonly SpeechRecognitionEngine _speechEngine;

        private bool _newRecognized;

        public SpeechProcessor(KinectManager kinectManager, string[] inCommands)
        {
            _newRecognized = false;
            Recognized = string.Empty;
            RecognizerInfo kinectInfo = null;
            foreach (var rec in SpeechRecognitionEngine.InstalledRecognizers())
                if (rec.AdditionalInfo.ContainsKey("Kinect") && rec.Culture.Name == DefaultStatics.SpeechCulture &&
                    rec.AdditionalInfo["Kinect"] == "True")
                {
                    kinectInfo = rec;
                    break;
                }

            _speechEngine = new SpeechRecognitionEngine(kinectInfo);

            var commands = new Choices();
            commands.Add(inCommands);

            var builder = new GrammarBuilder();
            if (kinectInfo != null)
                builder.Culture = kinectInfo.Culture;
            builder.Append(commands);

            _speechEngine.LoadGrammar(new Grammar(builder));

            if (kinectManager.Sensor != null)
            {
                var source = kinectManager.Sensor.AudioSource;
                source.BeamAngleMode = BeamAngleMode.Adaptive;
                var audioStream = source.Start();

                _speechEngine.SetInputToAudioStream(audioStream,
                    new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
                IsWorking = true;
            }
            else
            {
                try
                {
                    _speechEngine.SetInputToDefaultAudioDevice();
                    IsWorking = true;
                }
                catch
                {
                    IsWorking = false;
                    return;
                }
            }
            _speechEngine.RecognizeAsync(RecognizeMode.Multiple);
            _speechEngine.SpeechRecognized += speechEngine_SpeechRecognized;
            Logger.Trace("Speech Recognizer is working...");
        }

        private void speechEngine_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (!(e.Result.Confidence > DefaultStatics.SpeechRecognizeThreshold))
                return;

            Recognized = e.Result.Text;
            _newRecognized = true;
        }
    }
}