﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;

namespace AXKNGT_Szakdoga.Service.Kinect
{
    internal class KinectPositionDetector
    {
        public enum PositionDetectErrors
        {
            NoBody,
            TooClose,
            TooRight,
            TooLeft
        }

        public enum PositionDetectState
        {
            Initial,
            OutOfRange,
            Detect,
            NoDevice
        }

        public PositionDetectErrors Error { private set; get; }

        private const int MaxBuffer = 50;
        private readonly SkeletonStreamProcessor _skeletonStreamProcessor;
        private readonly float _centerDegree;
        private readonly float _betweenDegree;
        private readonly List<float> _jumpBuffer;

        private Vector3 _nowPos;
        private PositionDetectState _state;

        public KinectPositionDetector(SkeletonStreamProcessor skeletonStreamProcessor)
        {
            _skeletonStreamProcessor = skeletonStreamProcessor;

            DetectedPlayerPosition = -1;
            if (skeletonStreamProcessor.IsSensorAvaible)
                _state = PositionDetectState.Initial;
            else
                _state = PositionDetectState.NoDevice;

            _centerDegree = 4f;
            _betweenDegree = 1f;
            _jumpBuffer = new List<float>();
        }

        public int DetectedPlayerPosition { get; private set; }

        public bool IsJumped()
        {
            if (_jumpBuffer.Count < MaxBuffer)
                return false;

            var threshold = _jumpBuffer.Max() - 0.1f;

            if (_jumpBuffer[0] < threshold && _jumpBuffer[_jumpBuffer.Count - 1] < threshold)
                return true;

            return false;
        }

        public void ResetState()
        {
            _jumpBuffer.Clear();
            if (_skeletonStreamProcessor.IsSensorAvaible)
                _state = PositionDetectState.Initial;
            else
                _state = PositionDetectState.NoDevice;
        }

        public PositionDetectState GetState()
        {
            var body = _skeletonStreamProcessor.GetATrackedBody();

            if (!_skeletonStreamProcessor.IsSensorAvaible)
                return PositionDetectState.NoDevice;

            if (body != null)
            {
                var shoulder = body.Joints[JointType.ShoulderCenter];
                if (shoulder.TrackingState == JointTrackingState.Tracked && shoulder.Position.Z > 1.75f)
                {
                    _nowPos = new Vector3(shoulder.Position.X, shoulder.Position.Y, shoulder.Position.Z);
                    var deg = MathHelper.ToDegrees((float)Math.Atan2(_nowPos.X, _nowPos.Z));

                    if (_state != PositionDetectState.Initial)
                    {
                        if (DetectedPlayerPosition != 1 && Math.Abs(deg) < _centerDegree - _betweenDegree)
                            DetectedPlayerPosition = 1;
                        else if (DetectedPlayerPosition != 0 && deg < -(_centerDegree + _betweenDegree))
                            DetectedPlayerPosition = 0;
                        else if (DetectedPlayerPosition != 2 && deg > _centerDegree + _betweenDegree)
                            DetectedPlayerPosition = 2;

                        _state = PositionDetectState.Detect;
                        _jumpBuffer.Add(_nowPos.Y);
                        if (_jumpBuffer.Count > MaxBuffer)
                            _jumpBuffer.RemoveAt(0);
                    }
                    else
                    {
                        if (Math.Abs(deg) < _centerDegree)
                        {
                            DetectedPlayerPosition = 1;
                            _state = PositionDetectState.Detect;
                        }
                        else if (deg < 0)
                        {
                            Error = PositionDetectErrors.TooLeft;
                        }
                        else
                        {
                            Error = PositionDetectErrors.TooRight;
                        }
                    }
                }
                else if (_state != PositionDetectState.Initial)
                {
                    _state = PositionDetectState.OutOfRange;
                    Error = PositionDetectErrors.TooClose;
                }
            }
            else
            {
                if (_state != PositionDetectState.Initial)
                    _state = PositionDetectState.OutOfRange;

                Error = PositionDetectErrors.NoBody;
            }

            return _state;
        }
    }
}