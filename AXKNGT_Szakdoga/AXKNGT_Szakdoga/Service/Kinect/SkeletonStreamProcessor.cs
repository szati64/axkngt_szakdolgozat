﻿using System.Linq;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;

namespace AXKNGT_Szakdoga.Service.Kinect
{
    internal class SkeletonStreamProcessor : GameComponent
    {
        private readonly KinectManager _manager;

        private Skeleton[] _skeletonData;
        private bool _ujFrame;

        public SkeletonStreamProcessor(Game game, KinectManager manager)
            : base(game)
        {
            _manager = manager;
            _ujFrame = true;
        }

        public bool IsSensorAvaible
        {
            get
            {
                return _manager.Sensor != null &&
                       _manager.Status == KinectStatus.Connected;
            }
        }


        public Vector2 Mapper(SkeletonPoint point)
        {
            if (_manager.Sensor != null && _manager.Sensor.ColorStream != null)
            {
                var depthPt = _manager.Sensor.CoordinateMapper.MapSkeletonPointToColorPoint(point,
                    ColorImageFormat.RgbResolution640x480Fps30);
                return new Vector2(depthPt.X, depthPt.Y);
            }

            return Vector2.Zero;
        }

        public Vector2 MapToSize(SkeletonPoint point, float width, float height)
        {
            if (_manager.Sensor != null && _manager.Sensor.ColorStream != null)
            {
                var depthPt = _manager.Sensor.CoordinateMapper.MapSkeletonPointToColorPoint(point,
                    ColorImageFormat.RgbResolution640x480Fps30);

                var vx = width / 640;
                var vy = height / 480;

                return new Vector2(depthPt.X * vx, depthPt.Y * vy);
            }

            return Vector2.Zero;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (_manager.Sensor == null ||
                _manager.Status != KinectStatus.Connected)
                return;

            if (!_ujFrame)
                return;

            using (var frame = _manager.Sensor.SkeletonStream.OpenNextFrame(0))
            {
                if (frame == null)
                    return;

                if (_skeletonData == null || _skeletonData.Length != frame.SkeletonArrayLength)
                    _skeletonData = new Skeleton[frame.SkeletonArrayLength];

                frame.CopySkeletonDataTo(_skeletonData);
                _ujFrame = false;
            }
        }

        public Skeleton GetATrackedBody()
        {
            if (_skeletonData == null)
                return null;

            _ujFrame = true;
            return _skeletonData.FirstOrDefault(b => b.TrackingState == SkeletonTrackingState.Tracked);
        }

        public Skeleton[] GetSkeletonData()
        {
            _ujFrame = true;
            return _skeletonData;
        }
    }
}