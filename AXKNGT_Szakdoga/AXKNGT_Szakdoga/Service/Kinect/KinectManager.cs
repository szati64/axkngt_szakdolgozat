﻿using System.IO;
using System.Linq;
using Microsoft.Kinect;
using NLog;

namespace AXKNGT_Szakdoga.Service.Kinect
{
    internal class KinectManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public KinectSensor Sensor { private set; get; }
        public KinectStatus Status { private set; get; }

        public static string KinectConnectedText = "Kapcsolodva";
        public static string KinectDisconnectedText = "Az eszkozt nem sikerult elerni";

        public KinectManager()
        {
            KinectSensor.KinectSensors.StatusChanged += KinectSensors_StatusChanged;
            InitiKinect();
        }

        private void KinectSensors_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            if (e.Status != KinectStatus.Connected)
                e.Sensor.Stop();

            Status = e.Status;
            Logger.Trace("New Kinect status: {0}", Status);
            InitiKinect();
        }

        public void InitiKinect()
        {
            Sensor = KinectSensor.KinectSensors.FirstOrDefault();
            if (Sensor == null)
            {
                Status = KinectStatus.Disconnected;
                Logger.Trace("Kinect not found");
                return;
            }

            Status = Sensor.Status;

            if (Status != KinectStatus.Connected)
                return;

            //alap: 640x480 30fps
            Sensor.ColorStream.Enable();
            Sensor.DepthStream.Enable();
            Sensor.SkeletonStream.Enable();

            try
            {
                Sensor.Start();
                Logger.Warn("Kinect is working...");
            }
            catch (IOException)
            {
                Logger.Warn("Can't start the Kinect sensor loop");
                Sensor = null;
            }
        }

        public string GetStatus()
        {
            switch (Status)
            {
                case KinectStatus.Connected:
                    return KinectConnectedText;
                case KinectStatus.Disconnected:
                    return KinectDisconnectedText;
                default:
                    return string.Empty;
            }
        }


        public bool IsOn()
        {
            return Sensor != null;
        }
    }
}