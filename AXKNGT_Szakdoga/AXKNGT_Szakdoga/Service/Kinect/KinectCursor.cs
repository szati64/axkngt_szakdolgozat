﻿using System;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;

namespace AXKNGT_Szakdoga.Service.Kinect
{
    internal class KinectCursor : GameComponent
    {
        public enum KinectCursorStatus
        {
            NoBody,
            Left,
            Right,
            Push,
            NoKinect
        }

        public KinectCursorStatus Status { private set; get; }

        private readonly SkeletonStreamProcessor _skeletonStreamProcessor;
        private readonly Vector2 _cursorDif = Vector2.Zero;

        private JointType _defHand;
        private bool _isClicked;
        private bool _isMoved;
        private int _pushCount;
        private int _upCount;
        private int _downCount;

        public KinectCursor(Game game, SkeletonStreamProcessor skeletonStreamProcessor)
            : base(game)
        {
            _skeletonStreamProcessor = skeletonStreamProcessor;
            _isClicked = false;
            _pushCount = 0;

            _upCount = 0;
            _downCount = 0;
            _isMoved = false;
            _defHand = JointType.HandRight;
            Status = KinectCursorStatus.NoKinect;
        }

        public Vector2 CursorDif
        {
            get { return _cursorDif; }
        }

        public bool IsClicked
        {
            get
            {
                if (!_isClicked)
                    return false;

                _isClicked = false;
                _pushCount = -120;
                return true;
                
            }
        }


        public bool IsDown
        {
            get
            {
                if (_downCount >= 3 && !_isMoved)
                {
                    _isMoved = true;
                    return true;
                }
                return false;
            }
        }

        public bool IsUp
        {
            get
            {
                if (_upCount >= 3 && !_isMoved)
                {
                    _isMoved = true;
                    return true;
                }
                return false;
            }
        }

        private void UpdatePositions(Joint shoulder, Joint elbow, Joint hand)
        {
            var u = new Vector3(shoulder.Position.X - elbow.Position.X, shoulder.Position.Y - elbow.Position.Y,
                shoulder.Position.Z - elbow.Position.Z);
            var v = new Vector3(hand.Position.X - elbow.Position.X, hand.Position.Y - elbow.Position.Y,
                hand.Position.Z - elbow.Position.Z);


            var d =
                MathHelper.ToDegrees((float)Math.Acos((u.X * v.X + u.Y * v.Y + u.Z * v.Z) / (u.Length() * v.Length())));

            if (d > 140)
            {
                _downCount++;
                _upCount = 0;
            }
            else if (d < 90)
            {
                _upCount++;
                _downCount = 0;
            }
            else
            {
                _isMoved = false;
                _downCount = 0;
                _upCount = 0;
            }
        }

        public void UpdateHandsAndPositions(Skeleton body, JointType shouldert, JointType elbowt, JointType handt)
        {
            var shoulder = body.Joints[shouldert];
            var elbow = body.Joints[elbowt];
            var hand = body.Joints[handt];

            if (shoulder.TrackingState == JointTrackingState.Tracked &&
                hand.TrackingState == JointTrackingState.Tracked &&
                elbow.TrackingState == JointTrackingState.Tracked)
            {
                var handV = new Vector3(hand.Position.X, hand.Position.Y, hand.Position.Z);
                var centerV = new Vector3(shoulder.Position.X, shoulder.Position.Y, shoulder.Position.Z);


                if (handV.Length() + 0.1f < centerV.Length())
                {
                    UpdatePositions(shoulder, elbow, hand);

                    switch (handt)
                    {
                        case JointType.HandLeft:
                            Status = KinectCursorStatus.Left;
                            break;
                        case JointType.HandRight:
                            Status = KinectCursorStatus.Right;
                            break;
                    }
                }
            }
        }


        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var body = _skeletonStreamProcessor.GetATrackedBody();

            if (_pushCount < 0)
                _pushCount++;

            if (!_skeletonStreamProcessor.IsSensorAvaible)
            {
                Status = KinectCursorStatus.NoKinect;
                return;
            }

            if (body == null)
            {
                Status = KinectCursorStatus.NoBody;
                return;
            }

            if (body.Joints[JointType.ShoulderCenter].TrackingState == JointTrackingState.Tracked &&
                body.Joints[JointType.HandLeft].TrackingState == JointTrackingState.Tracked &&
                body.Joints[JointType.HandLeft].Position.Z < body.Joints[JointType.ShoulderCenter].Position.Z - 0.5f &&
                body.Joints[JointType.HandRight].TrackingState == JointTrackingState.Tracked &&
                body.Joints[JointType.HandRight].Position.Z <
                body.Joints[JointType.ShoulderCenter].Position.Z - 0.5f)
            {
                _pushCount++;
                if (_pushCount >= 5)
                    _isClicked = true;
                Status = KinectCursorStatus.Push;
            }
            else
            {
                if (_pushCount < 0)
                    _pushCount = 0;

                if (body.Joints[JointType.Head].TrackingState == JointTrackingState.Tracked &&
                    body.Joints[JointType.HandLeft].TrackingState == JointTrackingState.Tracked &&
                    body.Joints[JointType.HandLeft].Position.Y > body.Joints[JointType.Head].Position.Y + 0.2f)
                    _defHand = JointType.HandLeft;

                if (body.Joints[JointType.Head].TrackingState == JointTrackingState.Tracked &&
                    body.Joints[JointType.HandRight].TrackingState == JointTrackingState.Tracked &&
                    body.Joints[JointType.HandRight].Position.Y > body.Joints[JointType.Head].Position.Y + 0.2f)
                    _defHand = JointType.HandRight;

                if (_defHand == JointType.HandLeft)
                    UpdateHandsAndPositions(body, JointType.ShoulderLeft, JointType.ElbowLeft, JointType.HandLeft);
                else
                    UpdateHandsAndPositions(body, JointType.ShoulderRight, JointType.ElbowRight, JointType.HandRight);
            }
        }
    }
}