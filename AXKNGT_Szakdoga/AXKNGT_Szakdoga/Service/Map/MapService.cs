﻿using System.IO;
using AXKNGT_Szakdoga.Model.Map;
using Newtonsoft.Json;

namespace AXKNGT_Szakdoga.Service.Map
{
    internal class MapService
    {
        public static MapsEntity LoadMaps(string mapsPath)
        {
            var mapsInput = File.ReadAllText(mapsPath);
            return JsonConvert.DeserializeObject<MapsEntity>(mapsInput);
        }
    }
}
