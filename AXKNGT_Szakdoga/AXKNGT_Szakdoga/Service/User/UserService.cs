﻿using System.IO;
using System.Threading;
using AXKNGT_Szakdoga.Model.Map;
using AXKNGT_Szakdoga.Model.User;
using Newtonsoft.Json;
using NLog;

namespace AXKNGT_Szakdoga.Service.User
{
    internal class UserService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public bool IsSaving { private set; get; }

        private readonly string _savePath;

        public UserService(string savePath)
        {
            _savePath = savePath;
            IsSaving = false;
        }

        public ProgressEntity LoadProgress(MapsEntity maps)
        {
            if (!File.Exists(_savePath))
                return CreateNewProgressEntity(maps);
            
            var saveInput = File.ReadAllText(_savePath);
            var back = JsonConvert.DeserializeObject<ProgressEntity>(saveInput);

            return !IsProgressValid(back, maps) ? null : back;
        }

        public void SaveProgress(ProgressEntity playerProgress)
        {
            IsSaving = true;
            var progress = JsonConvert.SerializeObject(playerProgress, Formatting.Indented);
            if (File.Exists(_savePath))
                File.Delete(_savePath);
            File.AppendAllText(_savePath, progress);
            Logger.Trace("User progress saved to {0}", _savePath);

            Thread.Sleep(1500);
            IsSaving = false;
        }

        private ProgressEntity CreateNewProgressEntity(MapsEntity maps)
        {
            var newInstance = new ProgressEntity { Levels = new bool[maps.Difficulties.Length][] };
            for (var i = 0; i < maps.Difficulties.Length; i++)
            {
                newInstance.Levels[i] = new bool[maps.Levels[i] == null ? 0 : maps.Levels[i].Length];
                for (var j = 0; j < newInstance.Levels[i].Length; j++)
                    newInstance.Levels[i][j] = false;
            }

            return newInstance;
        }

        private bool IsProgressValid(ProgressEntity progress, MapsEntity maps)
        {
            if (maps.Difficulties.Length != progress.Levels.Length)
                return false;

            for (var i = 0; i < maps.Difficulties.Length; i++)
                if (maps.Levels[i].Length != progress.Levels[i].Length)
                    return false;

            return true;
        }
    }
}
