﻿using Microsoft.Xna.Framework;

namespace AXKNGT_Szakdoga.Presentation.Views
{
    public enum TextPosition
    {
        Left,
        Right,
        Center,
        LeftCenter,
        RightCenter
    }

    public delegate Vector2 DrawText(
        string text, Vector2 pos, float scale, float shadowScale, Color textColor, Color outlineColor, bool isBig,
        TextPosition textPosition);
}
