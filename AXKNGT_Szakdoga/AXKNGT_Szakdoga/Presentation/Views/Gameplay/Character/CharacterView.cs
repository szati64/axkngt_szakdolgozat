﻿using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.Views.Gameplay.Character
{
    internal class CharacterView
    {
        public Matrix View { set; get; }
        public Matrix Projection { set; get; }
        public Vector3 CameraPos { set; get; }

        public Vector3 DefaultPosition { set; get; }
        public Vector3 DefaultVelocity { set; get; }

        public Vector3 Position { set; get; }
        public Vector3 Velocity { set; get; }

        public float MaxSpeed { set; get; }
        public float Slow { set; get; }

        public float Gravity { set; get; }
        public bool OnGround { set; get; }

        public float MaximumRotateSpeed { set; get; }
        public float ModelRotate { set; get; }

        private readonly Microsoft.Xna.Framework.Graphics.Model _characterModel;
        private readonly bool _hasLightOnCharacter;
        private readonly Vector3 _cameraDir;
        private readonly Vector3 _cameraUp;

        private Vector3 _savedVelocity;

        public CharacterView(Game game, Vector3 cameraDir, Vector3 cameraUp, Matrix projection, Vector3 position)
        {
            _cameraDir = cameraDir;
            _cameraUp = cameraUp;
            Projection = projection;
            _characterModel = game.Content.Load<Microsoft.Xna.Framework.Graphics.Model>(DefaultStatics.CharacterModelContentPath);

            Gravity = float.Parse(((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["Gravity"],
                CultureInfo.InvariantCulture.NumberFormat);
            Slow = float.Parse(((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["Slowing"],
                CultureInfo.InvariantCulture.NumberFormat);
            MaxSpeed = float.Parse(((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["MaxSpeed"],
                CultureInfo.InvariantCulture.NumberFormat);
            Position = position;
            DefaultPosition = position;
            Velocity =
                new Vector3(0, 0,
                    -float.Parse(
                        ((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["DefaultSpeed"],
                        CultureInfo.InvariantCulture.NumberFormat));
            MaximumRotateSpeed =
                float.Parse(
                    ((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["MaximumRotateSpeed"],
                    CultureInfo.InvariantCulture.NumberFormat);
            ModelRotate = 0;
            OnGround = true;

            DefaultVelocity = Velocity;
            CameraPos = Vector3.Zero;

            CameraUpdate();

            _hasLightOnCharacter = bool.Parse(((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["HasLightOnCharacter"]);

        }

        public void Reset()
        {
            Position = DefaultPosition;
            Velocity = DefaultVelocity;
            OnGround = true;
            CameraUpdate();
        }

        public void AddSpeed(float speed)
        {
            if (Velocity.Z > -MaxSpeed)
                Velocity = new Vector3(Velocity.X, Velocity.Y, Velocity.Z - speed);
        }

        public void Jump()
        {
            if (OnGround)
            {
                Velocity = new Vector3(Velocity.X, float.Parse(((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["JumpForce"],
                        CultureInfo.InvariantCulture.NumberFormat), Velocity.Z);
                OnGround = false;
            }
        }

        public void UltraJump()
        {
            if (OnGround)
            {
                Velocity = new Vector3(Velocity.X, float.Parse(
                        ((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["JumpPlatformForce"],
                        CultureInfo.InvariantCulture.NumberFormat), Velocity.Z);
                OnGround = false;
            }
        }

        public void Fall()
        {
            if (OnGround)
            {
                OnGround = false;
                Velocity = new Vector3(Velocity.X, Velocity.Y - 0.2f, Velocity.Z);
            }
        }

        public void Stop()
        {
            _savedVelocity.Z = Velocity.Z;
            Velocity = new Vector3(Velocity.X, Velocity.Y, 0);
        }

        public void Start()
        {
            Velocity = new Vector3(Velocity.X, Velocity.Y, _savedVelocity.Z);
        }

        private void CameraUpdate()
        {
            CameraPos = new Vector3(Position.X, Position.Y + DefaultStatics.CameraDistanceY, Position.Z + DefaultStatics.CameraDistanceZ);

            View = Matrix.CreateLookAt(CameraPos,
                CameraPos + _cameraDir,
                _cameraUp);
        }

        public void Update()
        {
            if (!OnGround)
                Velocity = new Vector3(Velocity.X, Velocity.Y - Gravity, Velocity.Z);

            if (DefaultVelocity.Z > Velocity.Z)
                Velocity = DefaultVelocity.Z < Velocity.Z * Slow ? new Vector3(Velocity.X, Velocity.Y, DefaultVelocity.Z) : new Vector3(Velocity.X, Velocity.Y, Velocity.Z * Slow);

            Position = new Vector3(Position.X, Position.Y + Velocity.Y, Position.Z + Velocity.Z);

            var rotateSpeed = MaximumRotateSpeed * Velocity.Z / MaxSpeed;
            ModelRotate += rotateSpeed;
            if (ModelRotate >= MathHelper.Pi)
                ModelRotate = ModelRotate - MathHelper.Pi;

            CameraUpdate();
        }

        public void Draw()
        {
            const float scale = 0.5f;
            foreach (var mesh in _characterModel.Meshes)
            {
                foreach (var effect1 in mesh.Effects)
                {
                    var effect = (BasicEffect) effect1;
                    effect.World = Matrix.CreateRotationZ(MathHelper.Pi / 2) *
                                   Matrix.CreateRotationX(ModelRotate) *
                                   Matrix.CreateScale(scale) *
                                   Matrix.CreateTranslation(Position.X, Position.Y + (1f - scale), Position.Z);
                    effect.View = View;
                    effect.Projection = Projection;

                    if (_hasLightOnCharacter)
                        effect.EnableDefaultLighting();
                }

                mesh.Draw();
            }
        }

        public void SetOnGround(float yCoordinate)
        {
            Velocity = new Vector3(Velocity.X, 0, Velocity.Z);
            Position = new Vector3(Position.X, yCoordinate, Position.Z);
            OnGround = true;
            CameraUpdate();
        }

        public void SetOnGround(Vector3 coordinate)
        {
            Position = coordinate;
            OnGround = true;
            CameraUpdate();
        }
    }
}