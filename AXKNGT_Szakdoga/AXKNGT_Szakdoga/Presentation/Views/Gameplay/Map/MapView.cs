﻿using System;
using System.Collections.Generic;
using AXKNGT_Szakdoga.Model.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.Views.Gameplay.Map
{
    internal class MapView
    {
        private const float SeparatorWidth = 1;
        private readonly Color[] _levelColors = { Color.DarkOrange, Color.Green, Color.Purple };
        private readonly Color _obstacleColor = Color.Red;
        private readonly Color _separatorColor = Color.White;
        private readonly BackgroundView _backgroundViewModel;
        private readonly Vector3 _backgroundPos;
        private readonly GraphicsDevice _device;
        private readonly List<int> _finishLevels;
        private readonly List<Matrix> _finishPoss;
        private readonly QuadView[] _finishQuadsViewModel;
        private readonly QuadWithTextureView _jumpQuad;
        private readonly QuadWithTextureView _speedQuad;
        private readonly BasicEffect _mapEffect;
        private readonly ObstacleView _obstacleViewModel;
        private readonly ObstacleView _wall;
        private readonly float _obstacleHeight;
        private readonly RasterizerState _rasterizerState;
        private readonly QuadView _smallSeparator;
        private readonly QuadView _bigSeparator;
        private readonly QuadView _startQuadViewModel;
        private readonly QuadWithTextureView _startwText;
        private readonly QuadWithTextureView _finishwText;
        private readonly float _startZ;
        private readonly float _wallHeight;

        private int _levels;
        private MapLevelEntity _mapLevel;
        private MapLevelViewModel[] _mapLevelViewModels;
        private Vector2 _blockSize;
        private Matrix _startPos;

        public MapView(Game game, MapLevelEntity mapLevel, GraphicsDevice device, Vector2 blockSize, float wallHeight,
            float obstacleHeight, Texture2D backgroundText, float fov, float maxDepth,
            Vector3 cameraDir, Matrix view, Matrix projection)
        {
            _device = device;

            _rasterizerState = new RasterizerState {CullMode = CullMode.None};

            _mapLevel = mapLevel;
            _blockSize = blockSize;
            _wallHeight = wallHeight;
            _obstacleHeight = obstacleHeight;
            _startZ = -blockSize.Y / 2;

            var ratio = (float)MyGame.Width / MyGame.Height;
            var width = 2 * ((float)Math.Tan(fov / 2) * maxDepth);
            _backgroundViewModel = new BackgroundView(device, new Vector2(width, width / ratio), backgroundText, projection);

            var jumpTexture = game.Content.Load<Texture2D>(DefaultStatics.JumpTextureContentPath);
            var speedTexture = game.Content.Load<Texture2D>(DefaultStatics.SpeedTextureContentPath);
            var startTexture = game.Content.Load<Texture2D>(DefaultStatics.StartTextureContentPath);
            var finishTexture = game.Content.Load<Texture2D>(DefaultStatics.FinishTextureContentPath);

            _backgroundPos = new Vector3(0,
                (float)(Math.Tan(Math.Atan2(Math.Abs(cameraDir.Y), Math.Abs(cameraDir.Z)) / 2) * maxDepth),
                maxDepth - 1f - maxDepth * 0.1f);

            _mapEffect = new BasicEffect(device)
            {
                World = Matrix.Identity,
                Projection = projection,
                View = view,
                LightingEnabled = true,
                VertexColorEnabled = true
            };
            _mapEffect.DirectionalLight0.SpecularColor = Color.Black.ToVector3();
            _mapEffect.DirectionalLight0.DiffuseColor = Color.White.ToVector3();
            _mapEffect.DirectionalLight0.Enabled = true;
            _mapEffect.DirectionalLight0.Direction = new Vector3(1, -1, -1);

            _jumpQuad = new QuadWithTextureView(device, new Vector2(blockSize.X / 2, blockSize.Y / 2), jumpTexture, _mapEffect);
            _speedQuad = new QuadWithTextureView(device, new Vector2(blockSize.X / 2, blockSize.Y / 2), speedTexture, _mapEffect);
            _smallSeparator = new QuadView(device, new Vector2(SeparatorWidth / 4, 3 * blockSize.Y / 10), _separatorColor);
            _bigSeparator = new QuadView(device, new Vector2(SeparatorWidth / 2, blockSize.Y / 2), _separatorColor);
            _obstacleViewModel = new ObstacleView(device, new Vector2(blockSize.X / 2, obstacleHeight / 2), _obstacleColor);
            _wall = new ObstacleView(device, new Vector2(blockSize.X / 2, wallHeight / 2), _obstacleColor);

            var wideQuad = new Vector2((blockSize.X * 3 + SeparatorWidth) / 2, blockSize.Y / 2);
            _startwText = new QuadWithTextureView(device, wideQuad, startTexture, _mapEffect);
            _startQuadViewModel = new QuadView(device, wideQuad, _levelColors[mapLevel.StartLevel % 3]);

            _finishwText = new QuadWithTextureView(device, wideQuad, finishTexture, _mapEffect);
            _finishQuadsViewModel = new QuadView[_levelColors.Length];
            for (var i = 0; i < _finishQuadsViewModel.Length; i++)
                _finishQuadsViewModel[i] = new QuadView(device, wideQuad, _levelColors[i]);
            _finishPoss = new List<Matrix>();
            _finishLevels = new List<int>();

            LoadMap(mapLevel);
        }

        private void LoadMap(MapLevelEntity mapLevel)
        {
            _levels = mapLevel.Informations.GetLength(0);
            _mapLevelViewModels = new MapLevelViewModel[_levels];

            _startPos = Matrix.CreateTranslation(new Vector3(0, _wallHeight * mapLevel.StartLevel, _startZ));

            for (var level = 0; level < _levels; level++)
            {
                var tav = _startZ;
                _mapLevelViewModels[level] = new MapLevelViewModel
                {
                    BigQuadViewModel = new QuadView(_device, new Vector2(_blockSize.X / 2, _blockSize.Y / 2), _levelColors[level % 3]),
                    SmallQuadViewModel = new QuadView(_device, new Vector2(SeparatorWidth / 4, _blockSize.Y / 10), _levelColors[level % 3])
                };

                for (var i = 0; i < mapLevel.Informations[0, 0].Length; i++)
                {
                    var bigSepOnLevel = mapLevel.Informations.GetLength(1);
                    for (var j = 0; j < mapLevel.Informations.GetLength(1); j++)
                        if (mapLevel.Informations[level, j][i] != '0')
                        {
                            var tmp =
                                Matrix.CreateTranslation(
                                    new Vector3(-_blockSize.X - SeparatorWidth / 2 + j * (_blockSize.X + SeparatorWidth / 2),
                                        _wallHeight * level, tav));

                            if (level != mapLevel.StartLevel || i != 0)
                                if (i != mapLevel.Informations[0, 0].Length - 1)
                                {
                                    _mapLevelViewModels[level].BigQuads.Add(tmp);

                                    switch (mapLevel.Informations[level, j][i])
                                    {
                                        case '2':
                                            _mapLevelViewModels[level].Obstacles.Add(
                                                Matrix.CreateTranslation(
                                                    new Vector3(
                                                        -_blockSize.X - SeparatorWidth / 2 + j * (_blockSize.X + SeparatorWidth / 2),
                                                        _obstacleHeight / 2 + _wallHeight * level, tav)));
                                            break;
                                        case '3':
                                            _mapLevelViewModels[level].Walls.Add(
                                                Matrix.CreateTranslation(
                                                    new Vector3(
                                                        -_blockSize.X - SeparatorWidth / 2 + j * (_blockSize.X + SeparatorWidth / 2),
                                                        _wallHeight / 2 + _wallHeight * level, tav)));
                                            break;
                                        case '4':
                                            _mapLevelViewModels[level].JumpQuads.Add(tmp);
                                            break;
                                        case '5':
                                            _mapLevelViewModels[level].SpeedQuads.Add(tmp);
                                            break;
                                    }
                                }
                                else if (j == 1)
                                {
                                    _finishLevels.Add(level);
                                    _finishPoss.Add(tmp);
                                }
                        }
                        else
                        {
                            bigSepOnLevel--;
                        }

                    for (var k = 0; k < 2; k++)
                    {
                        if (bigSepOnLevel != 0)
                            _mapLevelViewModels[level].BigSeparators.Add(Matrix.CreateTranslation(
                                new Vector3(-SeparatorWidth - _blockSize.X * 1.5f + k * (SeparatorWidth * 2 + _blockSize.X * 3),
                                    _wallHeight * level, tav)));

                        if ((level != mapLevel.StartLevel || i != 0) && i != mapLevel.Informations[0, 0].Length - 1 &&
                            (mapLevel.Informations[level, k][i] != '0' || mapLevel.Informations[level, k + 1][i] != '0'))
                        {
                            var sepX = -SeparatorWidth / 4 - _blockSize.X / 2 + k * (SeparatorWidth / 2 + _blockSize.X);
                            _mapLevelViewModels[level].SmallQuads.Add(
                                Matrix.CreateTranslation(new Vector3(sepX, _wallHeight * level,
                                    tav + _blockSize.Y / 2 - _blockSize.Y / 10)));
                            _mapLevelViewModels[level].SmallSeparators.Add(
                                Matrix.CreateTranslation(new Vector3(sepX, _wallHeight * level, tav)));
                            _mapLevelViewModels[level].SmallQuads.Add(
                                Matrix.CreateTranslation(new Vector3(sepX, _wallHeight * level,
                                    tav - _blockSize.Y / 2 + _blockSize.Y / 10)));
                        }
                    }
                    tav -= _blockSize.Y;
                }
            }
        }


        public void Draw(Matrix view, Vector3 cameraPos)
        {
            _device.RasterizerState = _rasterizerState;

            _mapEffect.View = view;
            _backgroundViewModel.Draw(view, Matrix.CreateTranslation(cameraPos - _backgroundPos));

            if (_levels != 1)
                _mapEffect.Alpha = 0.7f;
            else
                _mapEffect.Alpha = 1f;

            _startQuadViewModel.Draw(_mapEffect, _startPos);
            _startwText.Draw(view, _startPos);

            for (var i = 0; i < _finishLevels.Count; i++)
            {
                _finishQuadsViewModel[_finishLevels[i] % _finishQuadsViewModel.Length].Draw(_mapEffect, _finishPoss[i]);
                _finishwText.Draw(view, _finishPoss[i]);
            }

            for (var level = 0; level < _levels; level++)
            {

                if (_levels != 1)
                    _mapEffect.Alpha = 0.7f;
                _wall.Draw(_mapEffect, _mapLevelViewModels[level].Walls);
                _obstacleViewModel.Draw(_mapEffect, _mapLevelViewModels[level].Obstacles);


                _bigSeparator.Draw(_mapEffect, _mapLevelViewModels[level].BigSeparators);
                _smallSeparator.Draw(_mapEffect, _mapLevelViewModels[level].SmallSeparators);
                _mapLevelViewModels[level].SmallQuadViewModel.Draw(_mapEffect, _mapLevelViewModels[level].SmallQuads);
                _mapLevelViewModels[level].BigQuadViewModel.Draw(_mapEffect, _mapLevelViewModels[level].BigQuads);

                _jumpQuad.Draw(view, _mapLevelViewModels[level].JumpQuads);
                _speedQuad.Draw(view, _mapLevelViewModels[level].SpeedQuads);
            }
        }

        public int GetLevelByPosition(Vector3 position)
        {
            var level = (int)(position.Y / _wallHeight);
            level = level >= _mapLevel.Informations.GetLength(0) ? _mapLevel.Informations.GetLength(0) - 1 : level;
            return level < 0 ? 0 : level;
        }

        public float GetLevelHeight(int level)
        {
            return level * _wallHeight;
        }

        public char GetTypeByPosition(Vector3 position)
        {
            if (position.Z <= -_blockSize.Y * _mapLevel.Informations[0, 0].Length)
                return '0';

            return
                _mapLevel.Informations[GetLevelByPosition(position), GetXByPosition(position)][GetZByPosition(position)];
        }

        public Vector3 GetCoordinateByPosition(Vector3 position)
        {
            return new Vector3(GetXByPosition(position), GetLevelByPosition(position), GetZByPosition(position));
        }

        public float GetUpperLevelCoordinate(Vector3 nowPosition)
        {
            var n = new Vector3(nowPosition.X, nowPosition.Y + _wallHeight, nowPosition.Z);
            return GetLevelByPosition(n) * _wallHeight;
        }

        public float GetLowerLevelCoordinate(Vector3 nowPosition)
        {
            var n = new Vector3(nowPosition.X, nowPosition.Y - _wallHeight, nowPosition.Z);
            return GetLevelByPosition(n) * _wallHeight;
        }

        public bool IsWon(Vector3 position)
        {
            return GetZByPosition(position) == _mapLevel.Informations[0, 0].Length - 1 && position.Y >= 0;
        }

        public float GetLaneCordinate(int lane)
        {
            return (_blockSize.X + SeparatorWidth / 2) * (lane - 1);
        }

        public int GetStartLevel()
        {
            return _mapLevel.StartLevel;
        }

        public Vector3 GetCenterPosition()
        {
            return new Vector3(GetLaneCordinate(1), GetLevelHeight(_levels / 2), 0);
        }

        public bool WillHeCollision(Vector3 position, Vector3 velocity)
        {
            var back = false;
            var type = GetTypeByPosition(position);
            var newz = Math.Abs(-GetZByPosition(position) * _blockSize.Y - _blockSize.Y / 2);

            if (newz >= Math.Abs(position.Z) && newz < Math.Abs(position.Z) + Math.Abs(velocity.Z))
                if (type == '2' &&
                    Math.Abs(position.Y) < Math.Abs(_obstacleHeight + _wallHeight * GetLevelByPosition(position)))
                    back = true;
                else if (type == '3' && Math.Abs(position.Y) < Math.Abs(_wallHeight * (GetLevelByPosition(position) + 1)))
                    back = true;

            return back;
        }

        private int GetXByPosition(Vector3 position)
        {
            return (int)((position.X + (_blockSize.X + SeparatorWidth / 2)) / (_blockSize.X + SeparatorWidth / 2));
        }

        private int GetZByPosition(Vector3 position)
        {
            return (int)(-position.Z / _blockSize.Y);
        }
    }
}