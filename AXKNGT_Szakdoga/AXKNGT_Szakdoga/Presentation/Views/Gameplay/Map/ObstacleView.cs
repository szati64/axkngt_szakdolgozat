﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.Views.Gameplay.Map
{
    internal class ObstacleView
    {
        private const int NumberOfVertices = 6;
        private readonly GraphicsDevice _device;
        private readonly VertexBuffer _vertexBuffer;
        private readonly VertexPositionColorNormal[] _vertices;

        public ObstacleView(GraphicsDevice device, Vector2 size, Color color)
        {
            _device = device;
            _vertices = new VertexPositionColorNormal[NumberOfVertices];
            _vertexBuffer = new VertexBuffer(
                device,
                typeof(VertexPositionColorNormal),
                NumberOfVertices,
                BufferUsage.None
            );

            InitializeCube(size, color);

            _vertexBuffer.SetData(_vertices);
        }

        private void InitializeCube(Vector2 size, Color color)
        {
            var leftTop = new Vector3(-size.X, size.Y, 0);
            var leftBottom = new Vector3(-size.X, -size.Y, 0);
            var rightTop = new Vector3(size.X, size.Y, 0);
            var rightBottom = new Vector3(size.X, -size.Y, 0);

            _vertices[0] = new VertexPositionColorNormal(leftTop, color, Vector3.UnitZ);
            _vertices[1] = new VertexPositionColorNormal(leftBottom, color, Vector3.UnitZ);
            _vertices[2] = new VertexPositionColorNormal(rightTop, color, Vector3.UnitZ);
            _vertices[3] = new VertexPositionColorNormal(leftBottom, color, Vector3.UnitZ);
            _vertices[4] = new VertexPositionColorNormal(rightBottom, color, Vector3.UnitZ);
            _vertices[5] = new VertexPositionColorNormal(rightTop, color, Vector3.UnitZ);
        }


        public void Draw(BasicEffect effect, List<Matrix> positions)
        {
            _device.SetVertexBuffer(_vertexBuffer);
            foreach (var matrix in positions)
            {
                effect.World = matrix;
                foreach (var pass in effect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    _device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
                }
            }
        }
    }
}