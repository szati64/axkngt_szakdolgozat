﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AXKNGT_Szakdoga.Presentation.Views.Gameplay.Map
{
    internal class MapLevelViewModel
    {
        public QuadView BigQuadViewModel { get; set; }
        public QuadView SmallQuadViewModel { get; set; }
        public List<Matrix> BigQuads { get; set; }
        public List<Matrix> SmallQuads { get; set; }
        public List<Matrix> SmallSeparators { get; set; }
        public List<Matrix> BigSeparators { get; set; }
        public List<Matrix> JumpQuads { get; set; }
        public List<Matrix> SpeedQuads { get; set; }
        public List<Matrix> Obstacles { get; set; }
        public List<Matrix> Walls { get; set; }

        public MapLevelViewModel()
        {
            BigQuads = new List<Matrix>();
            SmallQuads = new List<Matrix>();
            SmallSeparators = new List<Matrix>();
            BigSeparators = new List<Matrix>();
            JumpQuads = new List<Matrix>();
            SpeedQuads = new List<Matrix>();
            Obstacles = new List<Matrix>();
            Walls = new List<Matrix>();
        }
    }
}