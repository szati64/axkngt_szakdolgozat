﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.Views.Gameplay.Map
{
    internal class QuadWithTextureView
    {
        private const int NumberOfVertices = 6;
        private readonly GraphicsDevice _device;
        private readonly BasicEffect _effect;
        private readonly VertexBuffer _vertexBuffer;
        private readonly VertexPositionNormalTexture[] _verticesTexture;

        public QuadWithTextureView(GraphicsDevice device, Vector2 size, Texture2D texture, BasicEffect effect)
        {
            _device = device;
            _verticesTexture = new VertexPositionNormalTexture[NumberOfVertices];
            _vertexBuffer = new VertexBuffer(
                device,
                typeof(VertexPositionNormalTexture),
                NumberOfVertices,
                BufferUsage.None
            );

            InitializeCube(size);

            _effect = (BasicEffect)effect.Clone();
            _effect.Texture = texture;
            _effect.VertexColorEnabled = false;
            _effect.TextureEnabled = true;

            _vertexBuffer.SetData(_verticesTexture);
        }

        private void InitializeCube(Vector2 size)
        {
            var leftFront = new Vector3(-size.X, 0, size.Y);
            var leftBack = new Vector3(-size.X, 0, -size.Y);
            var rightFront = new Vector3(size.X, 0, size.Y);
            var rightBack = new Vector3(size.X, 0, -size.Y);

            _verticesTexture[0] = new VertexPositionNormalTexture(leftFront, Vector3.UnitY, new Vector2(0, 1));
            _verticesTexture[1] = new VertexPositionNormalTexture(leftBack, Vector3.UnitY, new Vector2(0, 0));
            _verticesTexture[2] = new VertexPositionNormalTexture(rightBack, Vector3.UnitY, new Vector2(1, 0));
            _verticesTexture[3] = new VertexPositionNormalTexture(leftFront, Vector3.UnitY, new Vector2(0, 1));
            _verticesTexture[4] = new VertexPositionNormalTexture(rightBack, Vector3.UnitY, new Vector2(1, 0));
            _verticesTexture[5] = new VertexPositionNormalTexture(rightFront, Vector3.UnitY, new Vector2(1, 1));
        }


        public void Draw(Matrix view, List<Matrix> positions)
        {
            _device.SetVertexBuffer(_vertexBuffer);
            _effect.View = view;

            foreach (var postion in positions)
            {
                _effect.World = postion;

                foreach (var pass in _effect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    _device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
                }
            }
        }

        public void Draw(Matrix view, Matrix position)
        {
            _device.SetVertexBuffer(_vertexBuffer);
            _effect.View = view;
            _effect.World = position;

            foreach (var pass in _effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                _device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
            }
        }
    }
}