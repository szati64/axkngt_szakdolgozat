﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.Views.Gameplay.Map
{
    internal class BackgroundView
    {
        private const int NumberOfVertices = 6;
        private readonly GraphicsDevice _device;
        private readonly BasicEffect _effect;
        private readonly VertexBuffer _vertexBuffer;
        private readonly VertexPositionNormalTexture[] _vertices;

        public BackgroundView(GraphicsDevice device, Vector2 size, Texture2D texture, Matrix projection)
        {
            _device = device;
            _vertices = new VertexPositionNormalTexture[NumberOfVertices];
            _vertexBuffer = new VertexBuffer(
                device,
                typeof(VertexPositionNormalTexture),
                NumberOfVertices,
                BufferUsage.None
            );

            _effect = new BasicEffect(device)
            {
                Projection = projection,
                TextureEnabled = true,
                Texture = texture
            };


            _effect.DirectionalLight0.SpecularColor = Color.Black.ToVector3();
            _effect.DirectionalLight0.DiffuseColor = Color.White.ToVector3();
            _effect.DirectionalLight0.Enabled = true;
            _effect.DirectionalLight0.Direction = new Vector3(1, -1, -1);
            _effect.LightingEnabled = true;

            InitializeCube(size);

            _vertexBuffer.SetData(_vertices);
        }

        private void InitializeCube(Vector2 size)
        {
            var leftTop = new Vector3(-size.X, size.Y, 0);
            var leftBottom = new Vector3(-size.X, -size.Y, 0);
            var rightTop = new Vector3(size.X, size.Y, 0);
            var rightBottom = new Vector3(size.X, -size.Y, 0);

            _vertices[0] = new VertexPositionNormalTexture(leftTop, Vector3.UnitZ, new Vector2(0, 0));
            _vertices[1] = new VertexPositionNormalTexture(leftBottom, Vector3.UnitZ, new Vector2(0, 1));
            _vertices[2] = new VertexPositionNormalTexture(rightTop, Vector3.UnitZ, new Vector2(1, 0));
            _vertices[3] = new VertexPositionNormalTexture(leftBottom, Vector3.UnitZ, new Vector2(0, 1));
            _vertices[4] = new VertexPositionNormalTexture(rightBottom, Vector3.UnitZ, new Vector2(1, 1));
            _vertices[5] = new VertexPositionNormalTexture(rightTop, Vector3.UnitZ, new Vector2(1, 0));
        }


        public void Draw(Matrix view, Matrix world)
        {
            _effect.View = view;
            _effect.World = world;

            _device.SetVertexBuffer(_vertexBuffer);

            var rasterizerState1 = new RasterizerState {CullMode = CullMode.None};
            _device.RasterizerState = rasterizerState1;

            foreach (var pass in _effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                _device.SamplerStates[0] = SamplerState.LinearClamp;
                _device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
            }
        }
    }
}