﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.Views.Gameplay.Map
{
    internal class QuadView
    {
        private const int NumberOfVertices = 6;
        private readonly GraphicsDevice _device;
        private readonly VertexBuffer _vertexBuffer;
        private readonly VertexPositionColorNormal[] _vertices;

        public QuadView(GraphicsDevice device, Vector2 size, Color color)
        {
            _device = device;
            _vertices = new VertexPositionColorNormal[NumberOfVertices];
            _vertexBuffer = new VertexBuffer(
                device,
                typeof(VertexPositionColorNormal),
                NumberOfVertices,
                BufferUsage.None
            );

            InitializeCube(size, color);

            _vertexBuffer.SetData(_vertices);
        }

        private void InitializeCube(Vector2 size, Color color)
        {
            var leftFront = new Vector3(-size.X, 0, size.Y);
            var leftBack = new Vector3(-size.X, 0, -size.Y);
            var rightFront = new Vector3(size.X, 0, size.Y);
            var rightBack = new Vector3(size.X, 0, -size.Y);


            _vertices[0] = new VertexPositionColorNormal(leftFront, color, Vector3.UnitY);
            _vertices[1] = new VertexPositionColorNormal(leftBack, color, Vector3.UnitY);
            _vertices[2] = new VertexPositionColorNormal(rightBack, color, Vector3.UnitY);
            _vertices[3] = new VertexPositionColorNormal(leftFront, color, Vector3.UnitY);
            _vertices[4] = new VertexPositionColorNormal(rightBack, color, Vector3.UnitY);
            _vertices[5] = new VertexPositionColorNormal(rightFront, color, Vector3.UnitY);
        }

        public void Draw(BasicEffect effect, List<Matrix> positions)
        {
            _device.SetVertexBuffer(_vertexBuffer);

            foreach (var position in positions)
            {
                effect.World = position;
                foreach (var pass in effect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    _device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
                }
            }
        }

        public void Draw(BasicEffect effect, Matrix world)
        {
            _device.SetVertexBuffer(_vertexBuffer);
            effect.World = world;
            foreach (var pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                _device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
            }
        }
    }
}