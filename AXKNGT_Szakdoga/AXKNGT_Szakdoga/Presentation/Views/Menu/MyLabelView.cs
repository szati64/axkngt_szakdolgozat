﻿using AXKNGT_Szakdoga.Presentation.APIs;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.Views.Menu
{
    internal class MyLabelView : IResizeable
    {
        public enum IconPosition
        {
            Left,
            LeftWithCenteredText,
            Right,
            None
        }

        public string Text { set; get; }
        public bool IsActive { set; get; }
        public IconPosition IconPos { set; get; }

        private readonly Texture2D _background;
        private readonly Texture2D _backgroundActive;
        private readonly DrawText _drawText;
        private readonly Texture2D _icon;
        private readonly Texture2D _iconActive;
        private readonly TextPosition _textPositioning;

        private Rectangle _iconPosition;
        private Rectangle _position;
        private Vector2 _textPosition;
        private int _width;
        private int _height;

        public MyLabelView(Texture2D background, Texture2D backgroundActive, Texture2D icon, Texture2D iconActive,
            string text, Rectangle position, DrawText drawText, IconPosition icoPos)
        {
            _background = background;
            _backgroundActive = backgroundActive;
            _icon = icon;
            _iconActive = iconActive;
            Text = text;
            _position = position;
            _drawText = drawText;
            IconPos = icoPos;

            _width = MyGame.Width;
            _height = MyGame.Height;

            if (icoPos == IconPosition.Left)
                _textPositioning = TextPosition.LeftCenter;
            else if (icoPos == IconPosition.Right)
                _textPositioning = TextPosition.RightCenter;
            else
                _textPositioning = TextPosition.Center;

            IsActive = false;

            GameSizeChanged();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsActive)
            {
                spriteBatch.Draw(_backgroundActive, _position, Color.White);
                if (IconPos != IconPosition.None)
                    spriteBatch.Draw(_iconActive, _iconPosition, Color.White);
                _drawText(Text, _textPosition, 1f, 2f, Color.Black, Color.Black, false, _textPositioning);
            }
            else
            {
                if (_background != null)
                    spriteBatch.Draw(_background, _position, Color.White);
                if (IconPos != IconPosition.None)
                    spriteBatch.Draw(_icon, _iconPosition, Color.White);
                _drawText(Text, _textPosition, 1f, 2f, Color.White, Color.Black, false, _textPositioning);
            }
        }

        public void GameSizeChanged()
        {
            var xscale = MyGame.Width / DefaultStatics.ScreenWidth;
            var yscale = MyGame.Height / DefaultStatics.ScreenHeight;

            _position = new Rectangle((int)(_position.X * (float)MyGame.Width / _width),
                (int)(_position.Y * (float)MyGame.Height / _height),
                (int)(_position.Width * (float)MyGame.Width / _width),
                (int)(_position.Height * (float)MyGame.Height / _height));

            const float iconGap = 30f;
            switch (IconPos)
            {
                case IconPosition.Left:
                    _iconPosition = new Rectangle((int)(_position.X + iconGap * xscale - xscale * (_icon.Width / 2)),
                        (int)(_position.Y + _position.Height / 2f - yscale * (_icon.Height / 2)),
                        (int)(xscale * _icon.Width), (int)(yscale * _icon.Height));

                    _textPosition = new Vector2(_position.X + 60f * xscale, _position.Y + _position.Height / 2 + 2f);
                    break;
                case IconPosition.LeftWithCenteredText:
                    _iconPosition = new Rectangle((int)(_position.X + iconGap * xscale - xscale * (_icon.Width / 2)),
                        (int)(_position.Y + _position.Height / 2f - yscale * (_icon.Height / 2)),
                        (int)(xscale * _icon.Width), (int)(yscale * _icon.Height));

                    _textPosition = new Vector2(_position.X + (float)_position.Width / 2,
                        _position.Y + (float)_position.Height / 2 + 2f);
                    break;
                case IconPosition.Right:
                    _iconPosition =
                        new Rectangle((int)(_position.X + _position.Width - iconGap * xscale - xscale * (_icon.Width / 2)),
                            (int)(_position.Y + _position.Height / 2f - yscale * (_icon.Height / 2)),
                            (int)(xscale * _icon.Width), (int)(yscale * _icon.Height));

                    _textPosition = new Vector2(_position.X + _position.Width - iconGap * 2f * xscale,
                        _position.Y + _position.Height / 2 + 2f * MyGame.Height / _height);
                    break;
                case IconPosition.None:
                    _textPosition = new Vector2(_position.X + (float)_position.Width / 2,
                        _position.Y + (float)_position.Height / 2 + 2f);
                    break;
            }

            _width = MyGame.Width;
            _height = MyGame.Height;
        }
    }
}