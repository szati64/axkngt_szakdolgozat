﻿using System.Collections.Generic;
using AXKNGT_Szakdoga.Presentation.Views.Menu;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.APIs
{
    internal abstract class MenuWithSelect : DrawableGameComponent, IResizeable
    {
        public List<MyLabelView> Buttons { set; get; }

        public MyLabelView ActiveButton
        {
            get
            {
                foreach (var button in Buttons)
                    if (button.IsActive) return button;
                return null;
            }
        }

        public int ActiveButtonIndex
        {
            get
            {
                return Buttons.IndexOf(ActiveButton);
            }
        }

        protected MenuWithSelect(Game game) : base(game)
        {
            Buttons = new List<MyLabelView>();
        }

        public void SetActiveTo(int active)
        {
            if (active < 0 || active >= Buttons.Count)
                return;

            for (var i = 0; i < Buttons.Count; i++)
                Buttons[i].IsActive = active == i;
        }

        public void GameSizeChanged()
        {
            foreach (var button in Buttons)
                button.GameSizeChanged();
        }

        public override void Draw(GameTime gameTime)
        {
            var spriteBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));

            foreach (var button in Buttons)
                button.Draw(spriteBatch);

            base.Draw(gameTime);
        }
    }
}
