﻿namespace AXKNGT_Szakdoga.Presentation.APIs
{
    internal interface IResizeable
    {
        void GameSizeChanged();
    }
}
