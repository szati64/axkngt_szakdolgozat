﻿using AXKNGT_Szakdoga.Presentation.APIs;
using AXKNGT_Szakdoga.Service.Kinect;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.KinectRenderers
{
    internal class DepthStreamRenderer : DrawableGameComponent, IResizeable
    {
        public Rectangle PositionSize { set; get; }

        private readonly KinectManager _manager;
        private readonly SkeletonStreamRenderer _ssr;

        private RenderTarget2D _backBuffer;
        private short[] _depthData;
        private Texture2D _depthTexture;
        private Effect _kinectDepthVisualizer;
        private bool _ujFrame;
        private int _width, _height;

        public DepthStreamRenderer(Game game, KinectManager manager, SkeletonStreamProcessor ssp)
            : base(game)
        {
            _ssr = new SkeletonStreamRenderer(game, ssp);
            _manager = manager;
            _ujFrame = true;

            _width = MyGame.Width;
            _height = MyGame.Height;
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            _kinectDepthVisualizer = Game.Content.Load<Effect>(DefaultStatics.KinectDepthVisualizerEffectPath);
        }

        public void GameSizeChanged()
        {
            PositionSize = new Rectangle((int) (PositionSize.X * (float) MyGame.Width / _width),
                (int) (PositionSize.Y * (float) MyGame.Height / _height),
                (int) (PositionSize.Width * (float) MyGame.Width / _width),
                (int) (PositionSize.Height * (float) MyGame.Height / _height));

            _width = MyGame.Width;
            _height = MyGame.Height;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (_manager.Sensor == null ||
                _manager.Status != KinectStatus.Connected)
                return;


            using (var frame = _manager.Sensor.DepthStream.OpenNextFrame(0))
            {
                if (frame == null)
                    return;

                if (_depthData == null || _depthData.Length != frame.PixelDataLength)
                {
                    _depthData = new short[frame.PixelDataLength];

                    _depthTexture = new Texture2D(
                        Game.GraphicsDevice,
                        frame.Width,
                        frame.Height,
                        false,
                        SurfaceFormat.Bgra4444);

                    _backBuffer = new RenderTarget2D(
                        Game.GraphicsDevice,
                        frame.Width,
                        frame.Height,
                        false,
                        SurfaceFormat.Color,
                        DepthFormat.None,
                        Game.GraphicsDevice.PresentationParameters.MultiSampleCount,
                        RenderTargetUsage.PreserveContents);
                }

                frame.CopyPixelDataTo(_depthData);
                _ujFrame = true;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            var spriteBatch = (SpriteBatch) Game.Services.GetService(typeof(SpriteBatch));

            if (_kinectDepthVisualizer == null)
                LoadContent();

            if (_depthTexture == null)
                return;

            if (_ujFrame)
            {
                Game.GraphicsDevice.SetRenderTarget(_backBuffer);
                Game.GraphicsDevice.Clear(ClearOptions.Target, Color.Black, 1.0f, 0);

                _depthTexture.SetData(_depthData);

                spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, _kinectDepthVisualizer);
                spriteBatch.Draw(_depthTexture, Vector2.Zero, Color.White);
                spriteBatch.End();

                _ssr.Draw(gameTime);

                Game.GraphicsDevice.SetRenderTargets(null);

                _ujFrame = false;
            }


            spriteBatch.Begin();
            spriteBatch.Draw(_backBuffer, PositionSize, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}