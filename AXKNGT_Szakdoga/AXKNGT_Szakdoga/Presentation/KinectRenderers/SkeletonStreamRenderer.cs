﻿using System;
using AXKNGT_Szakdoga.Service.Kinect;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.KinectRenderers
{
    internal class SkeletonStreamRenderer : DrawableGameComponent
    {
        private readonly SkeletonStreamProcessor _skeletonStreamProcessor;

        private Vector2 _boneOrigin;
        private Texture2D _boneTexture;
        private Vector2 _jointOrigin;
        private Texture2D _jointTexture;

        public SkeletonStreamRenderer(Game game, SkeletonStreamProcessor skeletonStreamProcessor)
            : base(game)
        {
            _skeletonStreamProcessor = skeletonStreamProcessor;
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            _jointTexture = Game.Content.Load<Texture2D>(DefaultStatics.JointIconContentPath);
            _jointOrigin = new Vector2(_jointTexture.Width / 2, _jointTexture.Height / 2);

            _boneTexture = Game.Content.Load<Texture2D>(DefaultStatics.BoneIconContentPath);
            _boneOrigin = new Vector2(0.5f, 0.0f);
        }

        private void DrawBone(SpriteBatch sb, JointCollection joints, JointType startJoint, JointType endJoint)
        {
            var start = _skeletonStreamProcessor.Mapper(joints[startJoint].Position);
            var diff = _skeletonStreamProcessor.Mapper(joints[endJoint].Position) - start;
            var scale = new Vector2(1.0f, diff.Length() / _boneTexture.Height);

            var angle = (float) Math.Atan2(diff.Y, diff.X) - MathHelper.PiOver2;

            var color = Color.LightGreen;
            if (joints[startJoint].TrackingState != JointTrackingState.Tracked ||
                joints[endJoint].TrackingState != JointTrackingState.Tracked)
                color = Color.Gray;

            sb.Draw(_boneTexture, start, null, color, angle, _boneOrigin, scale, SpriteEffects.None, 1.0f);
        }

        public override void Draw(GameTime gameTime)
        {
            if (_jointTexture == null)
                LoadContent();

            var skeleton = _skeletonStreamProcessor.GetATrackedBody();

            if (skeleton == null)
                return;

            var spriteBatch = (SpriteBatch) Game.Services.GetService(typeof(SpriteBatch));

            spriteBatch.Begin();

            switch (skeleton.TrackingState)
            {
                case SkeletonTrackingState.Tracked:
                    DrawBone(spriteBatch, skeleton.Joints, JointType.Head, JointType.ShoulderCenter);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.ShoulderCenter, JointType.ShoulderLeft);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.ShoulderCenter, JointType.ShoulderRight);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.ShoulderCenter, JointType.Spine);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.Spine, JointType.HipCenter);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.HipCenter, JointType.HipLeft);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.HipCenter, JointType.HipRight);

                    DrawBone(spriteBatch, skeleton.Joints, JointType.ShoulderLeft, JointType.ElbowLeft);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.ElbowLeft, JointType.WristLeft);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.WristLeft, JointType.HandLeft);

                    DrawBone(spriteBatch, skeleton.Joints, JointType.ShoulderRight, JointType.ElbowRight);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.ElbowRight, JointType.WristRight);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.WristRight, JointType.HandRight);

                    DrawBone(spriteBatch, skeleton.Joints, JointType.HipLeft, JointType.KneeLeft);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.KneeLeft, JointType.AnkleLeft);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.AnkleLeft, JointType.FootLeft);

                    DrawBone(spriteBatch, skeleton.Joints, JointType.HipRight, JointType.KneeRight);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.KneeRight, JointType.AnkleRight);
                    DrawBone(spriteBatch, skeleton.Joints, JointType.AnkleRight, JointType.FootRight);

                    foreach (Joint j in skeleton.Joints)
                    {
                        var jointColor = Color.Green;
                        if (j.TrackingState != JointTrackingState.Tracked)
                            jointColor = Color.Yellow;

                        spriteBatch.Draw(
                            _jointTexture,
                            _skeletonStreamProcessor.Mapper(j.Position),
                            null,
                            jointColor,
                            0.0f,
                            _jointOrigin,
                            1.0f,
                            SpriteEffects.None,
                            0.0f);
                    }

                    break;
                case SkeletonTrackingState.PositionOnly:
                    spriteBatch.Draw(
                        _jointTexture,
                        _skeletonStreamProcessor.Mapper(skeleton.Position),
                        null,
                        Color.Blue,
                        0.0f,
                        _jointOrigin,
                        1.0f,
                        SpriteEffects.None,
                        0.0f);
                    break;
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}