﻿using AXKNGT_Szakdoga.Presentation.APIs;
using AXKNGT_Szakdoga.Presentation.Views;
using AXKNGT_Szakdoga.Presentation.Views.Menu;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.States.MainMenu.Screens
{
    internal class TitleScreen : MenuWithSelect
    {
        public static string PlayButtonText = "PLAY";
        public static string MuteButtonText = "MUTE";
        public static string FullscreenButtonText = "FULLSCREEN";
        public static string ExitButtonText = "EXIT";

        public TitleScreen(Game game, DrawText drawText, Vector2 buttonSize, float gapBetweenButtons, Texture2D defBackg,
            Texture2D defBackgAct)
            : base(game)
        {
            var playNotActive = game.Content.Load<Texture2D>(DefaultStatics.PlayIconContentPath);
            var playActive = game.Content.Load<Texture2D>(DefaultStatics.PlayActiveIconContentPath);
            var fullscreenNotActive = game.Content.Load<Texture2D>(DefaultStatics.FullscreenIconContentPath);
            var fullscreenActive = game.Content.Load<Texture2D>(DefaultStatics.FullscreenActiveIconContentPath);
            var exitNotActive = game.Content.Load<Texture2D>(DefaultStatics.ExitIconContentPath);
            var exitActive = game.Content.Load<Texture2D>(DefaultStatics.ExitActiveIconContentPath);
            var muteNotActive = game.Content.Load<Texture2D>(DefaultStatics.MuteIconContentPath);
            var muteActive = game.Content.Load<Texture2D>(DefaultStatics.MuteActiveIconContentPath);

            var buttonPos = new Vector2(MyGame.Width - DefaultStatics.MenuMargin * (MyGame.Width / DefaultStatics.ScreenWidth) - buttonSize.X,
                MyGame.Height - DefaultStatics.MenuMargin * (MyGame.Height / DefaultStatics.ScreenHeight) - buttonSize.Y);

            var exitButton = new MyLabelView(defBackg, defBackgAct, exitNotActive, exitActive, ExitButtonText,
                new Rectangle((int)buttonPos.X, (int)buttonPos.Y, (int)buttonSize.X, (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.Right);
            buttonPos.Y -= gapBetweenButtons;

            var fullScreenButton = new MyLabelView(defBackg, defBackgAct, fullscreenNotActive, fullscreenActive, FullscreenButtonText,
                new Rectangle((int)buttonPos.X, (int)buttonPos.Y, (int)buttonSize.X, (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.Right);
            buttonPos.Y -= gapBetweenButtons;

            var muteButton = new MyLabelView(defBackg, defBackgAct, muteNotActive, muteActive, MuteButtonText,
                new Rectangle((int)buttonPos.X, (int)buttonPos.Y, (int)buttonSize.X, (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.Right);
            buttonPos.Y -= gapBetweenButtons;

            var playButton = new MyLabelView(defBackg, defBackgAct, playNotActive, playActive, PlayButtonText,
                new Rectangle((int)buttonPos.X, (int)buttonPos.Y, (int)buttonSize.X, (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.Right);

            Buttons.Add(playButton);
            Buttons.Add(muteButton);
            Buttons.Add(fullScreenButton);
            Buttons.Add(exitButton);
            SetActiveTo(0);
        }
    }
}