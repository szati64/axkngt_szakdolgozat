﻿using System.Collections.Generic;
using AXKNGT_Szakdoga.Presentation.APIs;
using AXKNGT_Szakdoga.Presentation.Views;
using AXKNGT_Szakdoga.Presentation.Views.Menu;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.States.MainMenu.Screens
{
    internal class MapSelectScreen : MenuWithSelect
    {
        public static string MapButtonText = "MAP";
        public static string BackButtonText = "BACK";

        public MapSelectScreen(Game game, DrawText drawText, Vector2 buttonSize, float gapBetweenButtons, Texture2D defBackg,
            Texture2D defBackgAct,
            Texture2D complete, Texture2D completeActive, Texture2D back, Texture2D backActive, int levels,
            IList<bool> progress)
            : base(game)
        {
            var buttonPos = new Vector2(MyGame.Width / 2 - buttonSize.X / 2,
                MyGame.Height / 2 - buttonSize.Y / 2 * (levels + 1));

            for (var i = 0; i < levels; i++)
            {
                var button = new MyLabelView(defBackg, defBackgAct, complete, completeActive, MapButtonText + " " + (i + 1),
                    new Rectangle((int)buttonPos.X, (int)buttonPos.Y, (int)buttonSize.X, (int)buttonSize.Y),
                    drawText,
                    progress[i] ? MyLabelView.IconPosition.LeftWithCenteredText : MyLabelView.IconPosition.None);
                buttonPos.Y += gapBetweenButtons;

                Buttons.Add(button);
            }

            var backButton = new MyLabelView(defBackg, defBackgAct, back, backActive, BackButtonText,
                new Rectangle((int)buttonPos.X, (int)buttonPos.Y, (int)buttonSize.X, (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.LeftWithCenteredText);
            Buttons.Add(backButton);

            SetActiveTo(0);
        }
    }
}