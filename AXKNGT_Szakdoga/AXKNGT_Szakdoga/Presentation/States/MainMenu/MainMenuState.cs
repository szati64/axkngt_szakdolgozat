﻿using System;
using AXKNGT_Szakdoga.Model.Map;
using AXKNGT_Szakdoga.Model.User;
using AXKNGT_Szakdoga.Presentation.States.MainMenu.Screens;
using AXKNGT_Szakdoga.Presentation.Views;
using AXKNGT_Szakdoga.Presentation.Views.Menu;
using AXKNGT_Szakdoga.Service.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.States.MainMenu
{
    internal class MainMenuState : DrawableGameComponent
    {
        public enum MainMenuScreens
        {
            Title,
            Difficulty,
            MapSelect
        }

        public static string LoadingText = "LOADING...";
        public static string SavingText = "SAVING...";
        public static string RecordActiveText = "SAY THE LABEL OF BUTTONS";
        public static string RecordNotFoundText = "NO RECORDING DEVICE FOUND";
        public static string KinectNoBodyText = "STAND IN FRONT OF THE KINECT";
        public static string KinectActiveHandLeftText = "ACTIVE HAND: LEFT";
        public static string KinectActiveHandRightText = "ACTIVE HAND: RIGHT";
        public static string KinectPushText = "PUSH";
        public static string KinectNotFoundText = "KINECT NOT FOUND";

        public MainMenuScreens ActiveMenuScreen { set; get; }
        public int SelectedDifficulty { set; get; }
        public int SelectedMap { set; get; }
        public DifficultyScreen DifficultyScreen { private set; get; }
        public TitleScreen TitleScreen { private set; get; }
        public MapSelectScreen[] MapSelectScreens { private set; get; }
        public Texture2D Background { private set; get; }
        public Rectangle BackgroundRec { private set; get; }
        public MyLabelView SaveText { private set; get; }

        private readonly KinectCursor _kinectCursor;
        private readonly MyLabelView _kinectText;
        private readonly MyLabelView _voiceText;

        private KinectCursor.KinectCursorStatus _lastStatus;

        public MainMenuState(Game game, DrawText drawText, bool isVoiceWorking, KinectCursor kinectCursor, MapsEntity maps,
            ProgressEntity playerProgress)
            : base(game)
        {
            BackgroundRec = new Rectangle(0, 0, MyGame.Width, MyGame.Height);

            Background = game.Content.Load<Texture2D>(DefaultStatics.MainMenuBackgroundContentPath);
            var defBackg = game.Content.Load<Texture2D>(DefaultStatics.ButtonBackgroundContentPath);
            var defBackgAct = game.Content.Load<Texture2D>(DefaultStatics.ButtonActiveBackgroundContentPath);
            var kinectIcon = game.Content.Load<Texture2D>(DefaultStatics.KinectIconContentPath);
            var micIcon = game.Content.Load<Texture2D>(DefaultStatics.MicIconContentPath);
            var complete = game.Content.Load<Texture2D>(DefaultStatics.CompleteIconContentPath);
            var completeActive = game.Content.Load<Texture2D>(DefaultStatics.CompleteActiveIconContentPath);
            var back = game.Content.Load<Texture2D>(DefaultStatics.BackIconContentPath);
            var backActive = game.Content.Load<Texture2D>(DefaultStatics.BackActiveIconContentPath);
            var saveIcon = game.Content.Load<Texture2D>(DefaultStatics.SaveIconContentPath);

            var buttonSize = new Vector2(MyGame.Width / DefaultStatics.ScreenWidth * DefaultStatics.ButtonWidth,
                MyGame.Height / DefaultStatics.ScreenHeight * DefaultStatics.ButtonHeight);
            var gapBetweenButtons = DefaultStatics.GapBetweenButtons / DefaultStatics.ScreenHeight * MyGame.Height + buttonSize.Y;


            TitleScreen = new TitleScreen(game, drawText, buttonSize, gapBetweenButtons, defBackg, defBackgAct);
            DifficultyScreen = new DifficultyScreen(game, drawText, buttonSize, gapBetweenButtons, defBackg, defBackgAct,
                back, backActive, maps.Difficulties);
            MapSelectScreens = new MapSelectScreen[maps.Difficulties.Length];
            
            for (var i = 0; i < maps.Difficulties.Length; i++)
                MapSelectScreens[i] = new MapSelectScreen(game, drawText, buttonSize, gapBetweenButtons,
                    defBackg, defBackgAct, complete, completeActive, back, backActive,
                    maps.Levels[i] == null ? 0 : maps.Levels[i].Length, playerProgress.Levels[i]);

            ActiveMenuScreen = MainMenuScreens.Title;

            var extraY = DefaultStatics.MenuMargin * (MyGame.Height / DefaultStatics.ScreenHeight);
            var statusWidthScale = 1.5f;

            _kinectCursor = kinectCursor;
            _lastStatus = kinectCursor.Status;

            _kinectText = new MyLabelView(defBackg, null, kinectIcon, null, GetKinectCursorMessage(_lastStatus),
                new Rectangle((int)(DefaultStatics.MenuMargin * (MyGame.Width / DefaultStatics.ScreenWidth)),
                    (int)extraY, (int)(buttonSize.X * statusWidthScale), (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.Left);
            extraY += gapBetweenButtons;

            _voiceText = new MyLabelView(defBackg, null, micIcon, null,
                isVoiceWorking ? RecordActiveText : RecordNotFoundText,
                new Rectangle((int)(DefaultStatics.MenuMargin * (MyGame.Width / DefaultStatics.ScreenWidth)),
                    (int)extraY, (int)(buttonSize.X * statusWidthScale), (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.Left);
            SaveText = new MyLabelView(defBackg, null, saveIcon, null,
                SavingText, new Rectangle((int)(DefaultStatics.MenuMargin * (MyGame.Width / DefaultStatics.ScreenWidth)),
                    (int)(MyGame.Height - DefaultStatics.MenuMargin * (MyGame.Height / DefaultStatics.ScreenHeight) - buttonSize.Y),
                    (int)buttonSize.X, (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.Left);
        }

        public int ActiveIndex
        {
            get
            {
                switch (ActiveMenuScreen)
                {
                    case MainMenuScreens.Title:
                        return TitleScreen.ActiveButtonIndex;
                    case MainMenuScreens.Difficulty:
                        return DifficultyScreen.ActiveButtonIndex;
                    case MainMenuScreens.MapSelect:
                        return MapSelectScreens[SelectedDifficulty].ActiveButtonIndex;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private static string GetKinectCursorMessage(KinectCursor.KinectCursorStatus status)
        {
            switch (status)
            {
                case KinectCursor.KinectCursorStatus.NoBody:
                    return KinectNoBodyText;
                case KinectCursor.KinectCursorStatus.Left:
                    return KinectActiveHandLeftText;
                case KinectCursor.KinectCursorStatus.Right:
                    return KinectActiveHandRightText;
                case KinectCursor.KinectCursorStatus.Push:
                    return KinectPushText;
                case KinectCursor.KinectCursorStatus.NoKinect:
                    return KinectNotFoundText;
                default:
                    return string.Empty;
            }
        }

        public void SetActiveTo(int active)
        {
            switch (ActiveMenuScreen)
            {
                case MainMenuScreens.Title:
                    TitleScreen.SetActiveTo(active);
                    break;
                case MainMenuScreens.Difficulty:
                    DifficultyScreen.SetActiveTo(active);
                    break;
                default:
                    MapSelectScreens[SelectedDifficulty].SetActiveTo(active);
                    break;
            }
        }

        public void SelectedCompleted()
        {
            MapSelectScreens[SelectedDifficulty].Buttons[SelectedMap].IconPos =
                MyLabelView.IconPosition.LeftWithCenteredText;
            MapSelectScreens[SelectedDifficulty].Buttons[SelectedMap].GameSizeChanged();
        }

        public void GameSizeChanged()
        {
            TitleScreen.GameSizeChanged();
            DifficultyScreen.GameSizeChanged();

            foreach (var mapSelectScreen in MapSelectScreens)
                mapSelectScreen.GameSizeChanged();

            _voiceText.GameSizeChanged();
            _kinectText.GameSizeChanged();
            SaveText.GameSizeChanged();

            BackgroundRec = new Rectangle(0, 0, MyGame.Width, MyGame.Height);
        }

        public bool Update(bool clicked, string recognized, bool goBack)
        {
            switch (ActiveMenuScreen)
            {
                case MainMenuScreens.Title:
                    if (clicked && TitleScreen.ActiveButton.Text == TitleScreen.PlayButtonText || recognized == TitleScreen.PlayButtonText)
                    {
                        ActiveMenuScreen = MainMenuScreens.Difficulty;
                        DifficultyScreen.SetActiveTo(0);
                    }
                    break;
                case MainMenuScreens.Difficulty:
                    if (clicked && DifficultyScreen.ActiveButton.Text == DifficultyScreen.BackButtonText || recognized == DifficultyScreen.BackButtonText ||
                        goBack)
                    {
                        ActiveMenuScreen = MainMenuScreens.Title;
                    }
                    else if (clicked)
                    {
                        for (var i = 0; i < DifficultyScreen.Buttons.Count; i++)
                            if (DifficultyScreen.Buttons[i].IsActive)
                                SelectedDifficulty = i;

                        MapSelectScreens[SelectedDifficulty].SetActiveTo(0);
                        ActiveMenuScreen = MainMenuScreens.MapSelect;
                    }
                    break;
                case MainMenuScreens.MapSelect:
                    if (clicked && MapSelectScreens[SelectedDifficulty].ActiveButton.Text == MapSelectScreen.BackButtonText ||
                        recognized == MapSelectScreen.BackButtonText || goBack)
                        ActiveMenuScreen = MainMenuScreens.Difficulty;
                    else if (clicked)
                        for (var i = 0; i < MapSelectScreens[SelectedDifficulty].Buttons.Count; i++)
                            if (MapSelectScreens[SelectedDifficulty].Buttons[i].IsActive)
                            {
                                SelectedMap = i;
                                return true;
                            }
                    break;
            }
            return false;
        }

        public override void Draw(GameTime gameTime)
        {
            var spriteBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));

            spriteBatch.Begin();
            spriteBatch.Draw(Background, BackgroundRec, Color.White);

            switch (ActiveMenuScreen)
            {
                case MainMenuScreens.Title:
                    TitleScreen.Draw(gameTime);
                    break;
                case MainMenuScreens.Difficulty:
                    DifficultyScreen.Draw(gameTime);
                    break;
                default:
                    MapSelectScreens[SelectedDifficulty].Draw(gameTime);
                    break;
            }

            _voiceText.Draw(spriteBatch);

            var currStatus = _kinectCursor.Status;
            if (_lastStatus != currStatus)
            {
                _kinectText.Text = GetKinectCursorMessage(currStatus);
                _lastStatus = currStatus;
            }
            _kinectText.Draw(spriteBatch);

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}