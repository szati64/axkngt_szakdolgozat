﻿using System.Collections.Generic;
using System.Linq;
using AXKNGT_Szakdoga.Presentation.APIs;
using AXKNGT_Szakdoga.Presentation.Views;
using AXKNGT_Szakdoga.Presentation.Views.Menu;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AXKNGT_Szakdoga.Presentation.States.Gameplay
{
    internal class InGameMenuState : DrawableGameComponent, IResizeable
    {
        public static string ContinueButtonText = "CONTINUE";
        public static string RetryButtonText = "RETRY";
        public static string MenuButtonText = "MENU";
        public static string PauseSpeechCommand = "PAUSE";
        public static string WinText = "VICTORY!";
        public static string LoseText = "YOU LOSE!";

        public List<MyLabelView> Buttons { set; get; }

        private readonly Texture2D _background;
        private readonly DrawText _drawText;

        private Rectangle _backgRect;
        private bool _continueShow;
        private bool _retryShow;
        private string _text;
        private Color _textColor;
        private Vector2 _textPosition;
        private int _width, _height;

        public MyLabelView ActiveButton
        {
            get
            {
                foreach (var button in Buttons)
                    if (button.IsActive) return button;
                return null;
            }
        }

        public int ActiveButtonIndex
        {
            get
            {
                return Buttons.IndexOf(ActiveButton);
            }
        }

        public InGameMenuState(Game game, DrawText drawText)
            : base(game)
        {
            _drawText = drawText;

            _background = game.Content.Load<Texture2D>(DefaultStatics.InGameMenuBackgroundContentPath);
            _backgRect = new Rectangle(0, 0, MyGame.Width, MyGame.Height);

            var defBackg = game.Content.Load<Texture2D>(DefaultStatics.ButtonBackgroundContentPath);
            var defBackgAct = game.Content.Load<Texture2D>(DefaultStatics.ButtonActiveBackgroundContentPath);

            var buttonSize = new Vector2(MyGame.Width / DefaultStatics.ScreenWidth * DefaultStatics.ButtonWidth,
                MyGame.Height / DefaultStatics.ScreenHeight * DefaultStatics.ButtonHeight);

            var buttonPos = new Vector2(MyGame.Width / 2 - buttonSize.X / 2,
                2f * MyGame.Height / 3 - buttonSize.Y);

            var gapBetweenButtons = DefaultStatics.GapBetweenButtons / DefaultStatics.ScreenHeight * MyGame.Height + buttonSize.Y;

            var continueButton = new MyLabelView(defBackg, defBackgAct, null, null, ContinueButtonText,
                new Rectangle((int)buttonPos.X, (int)buttonPos.Y, (int)buttonSize.X, (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.None);

            buttonPos.Y += gapBetweenButtons;

            var retryButton = new MyLabelView(defBackg, defBackgAct, null, null, RetryButtonText,
                new Rectangle((int)buttonPos.X, (int)buttonPos.Y, (int)buttonSize.X, (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.None);

            buttonPos.Y += gapBetweenButtons;

            var menuButton = new MyLabelView(defBackg, defBackgAct, null, null, MenuButtonText,
                new Rectangle((int)buttonPos.X, (int)buttonPos.Y, (int)buttonSize.X, (int)buttonSize.Y),
                drawText, MyLabelView.IconPosition.None);

            Buttons = new List<MyLabelView>();
            Buttons.Add(continueButton);
            Buttons.Add(retryButton);
            Buttons.Add(menuButton);

            _width = MyGame.Width;
            _height = MyGame.Height;

            _textPosition = new Vector2(MyGame.Width / 2, MyGame.Height / 3);

            SetActiveTo(0);
        }
        
        public void SetActiveTo(int active)
        {
            if (active < 0 || active > 2)
                return;

            if (active == 0 && !_continueShow)
                active++;

            if (active == 1 && !_retryShow)
                active++;
            
            for (var i = 0; i < Buttons.Count; i++)
                Buttons[i].IsActive = active == i;
        }

        public void GameSizeChanged()
        {
            foreach (var button in Buttons)
                button.GameSizeChanged();

            _textPosition = new Vector2(_textPosition.X * MyGame.Width / _width,
                _textPosition.Y * MyGame.Height / _height);
            _backgRect = new Rectangle(0, 0, MyGame.Width, MyGame.Height);

            _width = MyGame.Width;
            _height = MyGame.Height;
        }

        public void SetText(string text, Color color, bool continueShow, bool retryShow)
        {
            _text = text;
            _textColor = color;
            _continueShow = continueShow;
            _retryShow = retryShow;

            SetActiveTo(0);
        }

        public override void Draw(GameTime gameTime)
        {
            var spriteBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));

            spriteBatch.Begin();
            spriteBatch.Draw(_background, _backgRect, Color.White);

            _drawText(_text, _textPosition, 1f, 2f, _textColor, Color.Black, true, TextPosition.Center);

            if (_continueShow)
                Buttons.First(b => b.Text == ContinueButtonText).Draw(spriteBatch);

            if (_retryShow)
                Buttons.First(b => b.Text == RetryButtonText).Draw(spriteBatch);

            Buttons.First(b => b.Text == MenuButtonText).Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}