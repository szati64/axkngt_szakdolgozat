﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using AXKNGT_Szakdoga.Model.Map;
using AXKNGT_Szakdoga.Presentation.Views;
using AXKNGT_Szakdoga.Presentation.Views.Gameplay.Character;
using AXKNGT_Szakdoga.Presentation.Views.Gameplay.Map;
using AXKNGT_Szakdoga.Presentation.Views.Menu;
using AXKNGT_Szakdoga.Service.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace AXKNGT_Szakdoga.Presentation.States.Gameplay
{
    internal class GameplayState : DrawableGameComponent
    {
        public static string PauseText = "PAUSE";
        public static string KinectNoBodyText = "STAND IN FRONT OF THE KINECT";
        public static string KinectTooCloseText = "YOU ARE TOO CLOSE TO THE KINECT";
        public static string KinectTooRightText = "YOU ARE STANDING TOO FAR RIGHT";
        public static string KinectTooLeftText = "YOU ARE STANDING TOO FAR LEFT";

        public Song MapSong { private set; get; }

        private readonly float _addSpeed;
        private readonly CharacterView _characterViewModel;
        private readonly DrawText _drawText;
        private readonly MyLabelView _pauseIcon;
        private readonly KinectPositionDetector _kinectPositionDetector;
        private readonly MapView _map;

        private string _kinectErrorMessage;
        private Vector3 _lastCoordinate;
        private KeyboardState _lastKeyboardState;
        private KinectPositionDetector.PositionDetectErrors _lastKinectError;
        private KinectPositionDetector.PositionDetectState _lastPositionDetectState;
        private char _lastType;
        private TimeSpan _startAt;
        private double _timeLeft;

        public GameplayState(Game game, SkeletonStreamProcessor skeletonStreamProcessor, MapLevelEntity mapLevel, DrawText drawText,
            int width, int height)
            : base(game)
        {
            var buttonSize = new Vector2(width / DefaultStatics.ScreenWidth * DefaultStatics.ButtonWidth,
                height / DefaultStatics.ScreenHeight * DefaultStatics.ButtonHeight);

            var songIndex = new Random().Next(0, DefaultStatics.SongContentNames.Length);
            MapSong = game.Content.Load<Song>(DefaultStatics.SongsContentDirPath + DefaultStatics.SongContentNames[songIndex]);

            var micIcon = game.Content.Load<Texture2D>(DefaultStatics.MicIconContentPath);
            _pauseIcon = new MyLabelView(null, null, micIcon, null, PauseText, new Rectangle((int) (DefaultStatics.MenuMargin * (MyGame.Width / DefaultStatics.ScreenWidth)),
                    (int)(DefaultStatics.MenuMargin * (MyGame.Height / DefaultStatics.ScreenHeight)), (int)buttonSize.X, (int)buttonSize.Y), drawText, MyLabelView.IconPosition.Left);

            var cameraDir = new Vector3(0, -0.15f, -1);
            var cameraUp = Vector3.UnitY;
            var maxDepth = 500f;
            var minDepth = 0.1f;
            var fov = MathHelper.ToRadians(45);
            var projection = Matrix.CreatePerspectiveFieldOfView(fov,
                (float)MyGame.Width / MyGame.Height, minDepth, maxDepth);
            var blocksSize =
                new Vector2(
                    float.Parse(((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["BlockWidth"],
                        CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["BlockHeight"],
                        CultureInfo.InvariantCulture.NumberFormat));

            _drawText = drawText;
            _characterViewModel = new CharacterView(game, cameraDir, cameraUp, projection, new Vector3(0, 0, -blocksSize.Y / 2));
            _kinectPositionDetector = new KinectPositionDetector(skeletonStreamProcessor);
            _lastPositionDetectState = _kinectPositionDetector.GetState();

            var backgIndex = new Random().Next(0, DefaultStatics.GameplayBackgroundContentNames.Length);
            var backgroundTexture = Game.Content.Load<Texture2D>(DefaultStatics.GameplayBackgroundContentDirPath + DefaultStatics.GameplayBackgroundContentNames[backgIndex]);
            _map = new MapView(game, mapLevel, Game.GraphicsDevice, blocksSize,
                float.Parse(((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["WallHeight"],
                    CultureInfo.InvariantCulture.NumberFormat),
                float.Parse(((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["ObstacleHeight"],
                    CultureInfo.InvariantCulture.NumberFormat),
                backgroundTexture, fov, maxDepth, cameraDir, _characterViewModel.View, projection);

            _lastKeyboardState = Keyboard.GetState();
            _characterViewModel.SetOnGround(_map.GetLevelHeight(mapLevel.StartLevel));
            _startAt = TimeSpan.Zero;
            _lastType = _map.GetTypeByPosition(_characterViewModel.Position);
            _lastCoordinate = _map.GetCoordinateByPosition(_characterViewModel.Position);
            _addSpeed =
                float.Parse(((NameValueCollection)ConfigurationManager.GetSection("Gameplay"))["AcceleratorStrength"],
                    CultureInfo.InvariantCulture.NumberFormat);
            IsDied = false;

            _lastKinectError = _kinectPositionDetector.Error;
            _kinectErrorMessage = GetErrorMessage(_lastKinectError);
        }

        public bool IsDied { get; private set; }

        public bool IsWon
        {
            get { return _map.IsWon(_characterViewModel.Position); }
        }

        private static string GetErrorMessage(KinectPositionDetector.PositionDetectErrors error)
        {
            switch (error)
            {
                case KinectPositionDetector.PositionDetectErrors.NoBody:
                    return KinectNoBodyText;
                case KinectPositionDetector.PositionDetectErrors.TooClose:
                    return KinectTooCloseText;
                case KinectPositionDetector.PositionDetectErrors.TooRight:
                    return KinectTooRightText;
                case KinectPositionDetector.PositionDetectErrors.TooLeft:
                    return KinectTooLeftText;
                default:
                    return string.Empty;
            }
        }

        public void Reset()
        {
            ResetCountdown();
            IsDied = false;
            _kinectPositionDetector.ResetState();
            _characterViewModel.Reset();
            _characterViewModel.SetOnGround(_map.GetLevelHeight(_map.GetStartLevel()));
        }

        public void ResetCountdown()
        {
            _startAt = TimeSpan.Zero;
        }

        private void SwitchCharacterToLane(int lane)
        {
            _characterViewModel.Position = new Vector3
            {
                X = _map.GetLaneCordinate(lane),
                Y = _characterViewModel.Position.Y,
                Z = _characterViewModel.Position.Z
            };
        }

        private void GoLevelDown()
        {
            _characterViewModel.Position = new Vector3
            {
                X = _characterViewModel.Position.X,
                Y = _map.GetLowerLevelCoordinate(_characterViewModel.Position),
                Z = _characterViewModel.Position.Z
            };
        }

        public override void Update(GameTime gameTime)
        {
            var currentKeyboardState = Keyboard.GetState();
            var currentPds = _kinectPositionDetector.GetState();

            if (currentPds == KinectPositionDetector.PositionDetectState.OutOfRange)
                _startAt = TimeSpan.Zero;

            _lastPositionDetectState = currentPds;

            if (currentPds == KinectPositionDetector.PositionDetectState.NoDevice ||
                currentPds == KinectPositionDetector.PositionDetectState.Detect)
            {
                if (_timeLeft != 0)
                    _timeLeft = 0;

                if (_startAt == TimeSpan.Zero)
                    _startAt = gameTime.TotalGameTime + new TimeSpan(0, 0, 3);

                if (_startAt < gameTime.TotalGameTime)
                {
                    if (currentKeyboardState.IsKeyDown(Keys.Space))
                        _characterViewModel.Jump();
                    if (currentKeyboardState.IsKeyDown(Keys.A) || currentKeyboardState.IsKeyDown(Keys.D1))
                        SwitchCharacterToLane(0);
                    if (currentKeyboardState.IsKeyDown(Keys.S) || currentKeyboardState.IsKeyDown(Keys.D2))
                        SwitchCharacterToLane(1);
                    if (currentKeyboardState.IsKeyDown(Keys.D) || currentKeyboardState.IsKeyDown(Keys.D3))
                        SwitchCharacterToLane(2);

                    if (_lastKeyboardState.IsKeyDown(Keys.LeftControl) && currentKeyboardState.IsKeyUp(Keys.LeftControl))
                        GoLevelDown();

                    if (_lastKeyboardState.IsKeyDown(Keys.LeftShift) && currentKeyboardState.IsKeyUp(Keys.LeftShift))
                        _characterViewModel.UltraJump();

                    if (currentPds == KinectPositionDetector.PositionDetectState.Detect)
                        SwitchCharacterToLane(_kinectPositionDetector.DetectedPlayerPosition);

                    if (_kinectPositionDetector.IsJumped())
                        _characterViewModel.Jump();

                    var currentType = _map.GetTypeByPosition(_characterViewModel.Position);
                    var currentCoordinate = _map.GetCoordinateByPosition(_characterViewModel.Position);

                    if (_characterViewModel.OnGround)
                    {
                        if (currentType == '0')
                            _characterViewModel.Fall();

                        if (currentType == '4')
                            _characterViewModel.UltraJump();

                        if (currentType == '5' && _lastCoordinate != currentCoordinate)
                            _characterViewModel.AddSpeed(_addSpeed);
                    }

                    if (!_characterViewModel.OnGround && _lastType != '0' && _characterViewModel.Velocity.Y < 0 &&
                        (_characterViewModel.Position.Y > 0 && _characterViewModel.Position.Y + _characterViewModel.Velocity.Y < 0 ||
                         _lastCoordinate.Y != currentCoordinate.Y))
                        _characterViewModel.SetOnGround(_map.GetLevelHeight((int)_lastCoordinate.Y));

                    _lastCoordinate = currentCoordinate;
                    _lastType = currentType;

                    if (_map.WillHeCollision(_characterViewModel.Position, _characterViewModel.Velocity))
                    {
                        _characterViewModel.Stop();
                        IsDied = true;
                    }

                    _characterViewModel.Update();

                    if (_characterViewModel.Position.Y < -50f)
                        IsDied = true;
                }
                else
                {
                    _timeLeft = (_startAt - gameTime.TotalGameTime).TotalSeconds;
                }
            }

            _lastKeyboardState = currentKeyboardState;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            var spriteBatch = (SpriteBatch)Game.Services.GetService(typeof(SpriteBatch));

            _characterViewModel.Draw();
            _map.Draw(_characterViewModel.View, _characterViewModel.CameraPos);

            spriteBatch.Begin();

            _pauseIcon.Draw(spriteBatch);

            if (_lastPositionDetectState == KinectPositionDetector.PositionDetectState.Initial ||
                _lastPositionDetectState == KinectPositionDetector.PositionDetectState.OutOfRange)
            {
                var currentKinectError = _kinectPositionDetector.Error;
                if (currentKinectError != _lastKinectError)
                {
                    _kinectErrorMessage = GetErrorMessage(currentKinectError);
                    _lastKinectError = currentKinectError;
                }

                _drawText(_kinectErrorMessage, new Vector2(MyGame.Width / 2, MyGame.Height / 3), 1.5f, 2f, Color.Red,
                    Color.White, true, TextPosition.Center);
            }

            if (_timeLeft != 0 && (_lastPositionDetectState == KinectPositionDetector.PositionDetectState.NoDevice ||
                                  _lastPositionDetectState == KinectPositionDetector.PositionDetectState.Detect))
                _drawText(Math.Ceiling(_timeLeft).ToString(), new Vector2(MyGame.Width / 2, MyGame.Height / 3),
                    (float)(_timeLeft - Math.Floor(_timeLeft) + 1f),
                    (float)(_timeLeft - Math.Floor(_timeLeft) + 1f) + 1f, Color.White, Color.Black, true,
                    TextPosition.Center);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}