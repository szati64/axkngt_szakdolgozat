﻿namespace AXKNGT_Szakdoga.Model.Map
{
    internal class MapsEntity
    {
        public string[] Difficulties { set; get; }
        public MapLevelEntity[][] Levels { set; get; }
    }
}