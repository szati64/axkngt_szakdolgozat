﻿namespace AXKNGT_Szakdoga.Model.Map
{
    internal class MapLevelEntity
    {
        public int StartLevel { set; get; }
        public string[,] Informations { set; get; }
    }
}
