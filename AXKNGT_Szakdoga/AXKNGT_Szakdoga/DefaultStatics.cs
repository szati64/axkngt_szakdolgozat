﻿namespace AXKNGT_Szakdoga
{
    internal class DefaultStatics
    {
        public static readonly float ScreenWidth = 1920f;
        public static readonly float ScreenHeight = 1080f;
        public static readonly float MenuMargin = 25f;

        public static readonly string ButtonBackgroundContentPath = "Menu/defMenuBackg";
        public static readonly string ButtonActiveBackgroundContentPath = "Menu/defMenuBackgActive";
        public static readonly float ButtonWidth = 500f;
        public static readonly float ButtonHeight = 55f;
        public static readonly float GapBetweenButtons = 20f;

        public static readonly int KinectCamWidth = 240;
        public static readonly int KinectCamHeight = 180;

        public static readonly string InGameMenuBackgroundContentPath = "Menu/InGameMenuBackg";
        public static readonly string MainMenuBackgroundContentPath = "Menu/MainMenuBackground";

        public static readonly string KinectIconContentPath = "Menu/kinectIcon";
        public static readonly string MicIconContentPath = "Menu/micIcon";
        public static readonly string CompleteIconContentPath = "Menu/complete";
        public static readonly string CompleteActiveIconContentPath = "Menu/completeA";
        public static readonly string BackIconContentPath = "Menu/back";
        public static readonly string BackActiveIconContentPath = "Menu/backA";
        public static readonly string SaveIconContentPath = "Menu/save";

        public static readonly string PlayIconContentPath = "Menu/play";
        public static readonly string PlayActiveIconContentPath = "Menu/playA";
        public static readonly string FullscreenIconContentPath = "Menu/fullscreen";
        public static readonly string FullscreenActiveIconContentPath = "Menu/fullscreenA";
        public static readonly string ExitIconContentPath = "Menu/exit";
        public static readonly string ExitActiveIconContentPath = "Menu/exitA";
        public static readonly string MuteIconContentPath = "Menu/mute";
        public static readonly string MuteActiveIconContentPath = "Menu/muteA";

        public static readonly string JointIconContentPath = "Kinect/Joint";
        public static readonly string BoneIconContentPath = "Kinect/Bone";
        public static readonly string KinectDepthVisualizerEffectPath = "Kinect/KinectDepthVisualizer";
        public static readonly string KinectColorVisualizerEffectPath = "Kinect/KinectColorVisualizer";

        public static readonly string SongsContentDirPath = "Songs/";
        public static readonly string[] SongContentNames = { "Australia Waltzing Matilda", "Spanish Fire", "Unrelenting" };
        public static readonly string GameplayBackgroundContentDirPath = "Gameplay/";
        public static readonly string[] GameplayBackgroundContentNames = { "background", "backg2", "backg3" };
        public static readonly string CharacterModelContentPath = "Gameplay/ball";

        public static readonly string JumpTextureContentPath = "Gameplay/jumpIcon";
        public static readonly string SpeedTextureContentPath = "Gameplay/speedIcon";
        public static readonly string StartTextureContentPath = "Gameplay/start";
        public static readonly string FinishTextureContentPath = "Gameplay/finish";

        public static readonly string MainThemeSongPath = "Songs/Storm Chasers";
        public static readonly string BigFontContentPath = "Fonts/BigFont";
        public static readonly string MenuFontContentPath = "Fonts/MenuFont";

        public static readonly string SpeechCulture = "en-US";
        public static readonly float SpeechRecognizeThreshold = 0.5f;

        public static readonly float CameraDistanceY = 3f;
        public static readonly float CameraDistanceZ = 6f;



    }
}
