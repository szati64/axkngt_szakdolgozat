using System;
using System.Configuration;
using System.Threading;
using AXKNGT_Szakdoga.Model.Map;
using AXKNGT_Szakdoga.Model.User;
using AXKNGT_Szakdoga.Presentation.KinectRenderers;
using AXKNGT_Szakdoga.Presentation.States.Gameplay;
using AXKNGT_Szakdoga.Presentation.States.MainMenu;
using AXKNGT_Szakdoga.Presentation.States.MainMenu.Screens;
using AXKNGT_Szakdoga.Presentation.Views;
using AXKNGT_Szakdoga.Service.Kinect;
using AXKNGT_Szakdoga.Service.Map;
using AXKNGT_Szakdoga.Service.Speech;
using AXKNGT_Szakdoga.Service.User;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using NLog;

namespace AXKNGT_Szakdoga
{
    public class MyGame : Game
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private enum GameState
        {
            MainMenu,
            Loading,
            Gameplay,
            Lose,
            Victory,
            PauseMenu
        }
        
        public static int Width { private set; get; }
        public static int Height { private set; get; }

        private readonly Color _defaultBackColor = Color.CornflowerBlue;
        private readonly GraphicsDeviceManager _graphics;
        private readonly ColorStreamRenderer _colorStreamRenderer;
        private readonly DepthStreamRenderer _depthStreamRenderer;
        private readonly KinectCursor _kinectCursor;
        private readonly SkeletonStreamProcessor _skeletonStreamProcessor;
        private readonly SpeechProcessor _speechProcessor;
        private readonly UserService _userService;

        private SpriteFont _bigFont;
        private GameplayState _gameplayState;
        private GameState _gameState;
        private InGameMenuState _inGameMenuState;
        private bool _isLoading;
        private KeyboardState _lastKeyboardState;
        private MainMenuState _mainMenuState;
        private Song _mainThemeSong;
        private MapsEntity _maps;
        private SpriteFont _menuFont;
        private ProgressEntity _playerProgress;
        private SpriteBatch _spriteBatch;

        public MyGame(string saveDirPath)
        {
            Width = Convert.ToInt32(ConfigurationManager.AppSettings["Width"]);
            Height = Convert.ToInt32(ConfigurationManager.AppSettings["Height"]);

            _graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = Width,
                PreferredBackBufferHeight = Height,
                IsFullScreen = Convert.ToBoolean(ConfigurationManager.AppSettings["FullScreen"]),
                SynchronizeWithVerticalRetrace = Convert.ToBoolean(ConfigurationManager.AppSettings["Vsync"]),
                PreferMultiSampling = Convert.ToBoolean(ConfigurationManager.AppSettings["MultiSampling"])
            };
            _graphics.PreparingDeviceSettings += graphics_PreparingDeviceSettings;
            _graphics.ApplyChanges();

            Content.RootDirectory = "Content";

            var kinectManager = new KinectManager();
            _skeletonStreamProcessor = new SkeletonStreamProcessor(this, kinectManager);
            _kinectCursor = new KinectCursor(this, _skeletonStreamProcessor);
            _speechProcessor = new SpeechProcessor(kinectManager,
                new[]{TitleScreen.PlayButtonText, TitleScreen.MuteButtonText, TitleScreen.FullscreenButtonText, TitleScreen.ExitButtonText,
                    DifficultyScreen.BackButtonText,
                    GameplayState.PauseText,
                    InGameMenuState.ContinueButtonText, InGameMenuState.RetryButtonText, InGameMenuState.MenuButtonText});
            _userService = new UserService(saveDirPath);

            _colorStreamRenderer = new ColorStreamRenderer(this, kinectManager, _skeletonStreamProcessor);
            _depthStreamRenderer = new DepthStreamRenderer(this, kinectManager, _skeletonStreamProcessor);

            _gameState = GameState.MainMenu;
        }

        protected override void Initialize()
        {
            var cameraGap = (int)(Width / DefaultStatics.ScreenWidth * DefaultStatics.MenuMargin);
            var cameraWidth = (int)(Width / DefaultStatics.ScreenWidth * DefaultStatics.KinectCamWidth);
            var cameraHeight = (int)(Height / DefaultStatics.ScreenHeight * DefaultStatics.KinectCamHeight);
            var streamRendererPos = Width - cameraWidth - cameraGap;
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ShowColorStream"]))
            {
                _colorStreamRenderer.PositionSize = new Rectangle(streamRendererPos,
                    cameraGap, cameraWidth, cameraHeight);
                streamRendererPos -= cameraWidth + cameraGap;
                Components.Add(_colorStreamRenderer);
            }
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["ShowDepthStream"]))
            {
                _depthStreamRenderer.PositionSize = new Rectangle(streamRendererPos,
                    cameraGap, cameraWidth, cameraHeight);
                Components.Add(_depthStreamRenderer);
            }

            Components.Add(_skeletonStreamProcessor);

            _lastKeyboardState = Keyboard.GetState();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            Services.AddService(typeof(SpriteBatch), _spriteBatch);

            _mainThemeSong = Content.Load<Song>(DefaultStatics.MainThemeSongPath);
            _bigFont = Content.Load<SpriteFont>(DefaultStatics.BigFontContentPath);
            _menuFont = Content.Load<SpriteFont>(DefaultStatics.MenuFontContentPath);

            _maps = MapService.LoadMaps(ConfigurationManager.AppSettings["MapsFile"]);

            var userProgress = _userService.LoadProgress(_maps);
            if (userProgress == null)
                throw new Exception("User progrssion file is invalid!");

            _playerProgress = userProgress;

            _mainMenuState = new MainMenuState(this, DrawText, _speechProcessor.IsWorking, _kinectCursor, _maps, _playerProgress);
            _inGameMenuState = new InGameMenuState(this, DrawText);

            MediaPlayer.IsRepeating = true;
            MediaPlayer.IsMuted = Convert.ToBoolean(ConfigurationManager.AppSettings["Mute"]);
            MediaPlayer.Play(_mainThemeSong);
        }

        protected override void Update(GameTime gameTime)
        {
            if (!IsActive)
                return;

            base.Update(gameTime);

            var recognized = string.Empty;
            if (_speechProcessor.IsNewRecognized)
                recognized = _speechProcessor.Recognized;

            var currentKeyboardState = Keyboard.GetState();
            if (_gameState == GameState.MainMenu)
            {
                _kinectCursor.Update(gameTime);
                var clicked = _kinectCursor.IsClicked;
                var kinectUp = _kinectCursor.IsUp;
                var kinectDown = _kinectCursor.IsDown;

                if (_lastKeyboardState.IsKeyDown(Keys.Up) && currentKeyboardState.IsKeyUp(Keys.Up) || kinectUp)
                    _mainMenuState.SetActiveTo(_mainMenuState.ActiveIndex - 1);

                if (_lastKeyboardState.IsKeyDown(Keys.Down) && currentKeyboardState.IsKeyUp(Keys.Down) || kinectDown)
                    _mainMenuState.SetActiveTo(_mainMenuState.ActiveIndex + 1);

                if (_lastKeyboardState.IsKeyDown(Keys.Enter) && currentKeyboardState.IsKeyUp(Keys.Enter))
                    clicked = true;

                if (_mainMenuState.ActiveMenuScreen == MainMenuState.MainMenuScreens.Title)
                {
                    var selectedButton = _mainMenuState.TitleScreen.ActiveButton;

                    if (clicked && selectedButton.Text == TitleScreen.ExitButtonText ||
                        recognized == TitleScreen.ExitButtonText)
                        Exit();

                    if (clicked && selectedButton.Text == TitleScreen.MuteButtonText ||
                        recognized == TitleScreen.MuteButtonText)
                    {
                        MediaPlayer.IsMuted = !MediaPlayer.IsMuted;
                        if (!MediaPlayer.IsMuted)
                            MediaPlayer.MoveNext();
                    }

                    if (clicked && selectedButton.Text == TitleScreen.FullscreenButtonText ||
                        recognized == TitleScreen.FullscreenButtonText)
                        ToggleFullScreen();
                }

                if (_mainMenuState.Update(clicked, recognized,
                    _lastKeyboardState.IsKeyDown(Keys.Escape) && currentKeyboardState.IsKeyUp(Keys.Escape)))
                {
                    _mainMenuState.ActiveMenuScreen = MainMenuState.MainMenuScreens.Title;
                    _mainMenuState.SetActiveTo(0);
                    _gameState = GameState.Loading;
                    new Thread(() => LoadGame(_maps.Levels[_mainMenuState.SelectedDifficulty][_mainMenuState.SelectedMap])).Start();

                    _isLoading = true;
                }
            }
            else if (_gameState == GameState.Loading && !_isLoading)
            {
                MediaPlayer.Play(_gameplayState.MapSong);
                _gameState = GameState.Gameplay;
            }
            else if (_gameState == GameState.Gameplay)
            {
                _gameplayState.Update(gameTime);

                if (_gameplayState.IsDied)
                {
                    _gameState = GameState.Lose;
                    _inGameMenuState.SetText(InGameMenuState.LoseText, Color.Red, false, true);
                }
                else if (_gameplayState.IsWon)
                {
                    _gameState = GameState.Victory;
                    _inGameMenuState.SetText(InGameMenuState.WinText, Color.Green, false, false);
                    _mainMenuState.SelectedCompleted();
                    _playerProgress.Levels[_mainMenuState.SelectedDifficulty][_mainMenuState.SelectedMap] = true;
                    new Thread(() => _userService.SaveProgress(_playerProgress)).Start();
                }
                else if (_lastKeyboardState.IsKeyDown(Keys.Escape) && currentKeyboardState.IsKeyUp(Keys.Escape) ||
                         recognized == InGameMenuState.PauseSpeechCommand)
                {
                    _gameState = GameState.PauseMenu;
                    _inGameMenuState.SetText(string.Empty, Color.Blue, true, true);
                }
            }
            else if (_gameState == GameState.Victory || _gameState == GameState.Lose || _gameState == GameState.PauseMenu)
            {
                _kinectCursor.Update(gameTime);
                var clicked = _kinectCursor.IsClicked;
                var kinectUp = _kinectCursor.IsUp;
                var kinectDown = _kinectCursor.IsDown;

                if (_lastKeyboardState.IsKeyDown(Keys.Up) && currentKeyboardState.IsKeyUp(Keys.Up) || kinectUp)
                    _inGameMenuState.SetActiveTo(_inGameMenuState.ActiveButtonIndex - 1);

                if (_lastKeyboardState.IsKeyDown(Keys.Down) && currentKeyboardState.IsKeyUp(Keys.Down) || kinectDown)
                    _inGameMenuState.SetActiveTo(_inGameMenuState.ActiveButtonIndex + 1);

                if (_lastKeyboardState.IsKeyDown(Keys.Enter) && currentKeyboardState.IsKeyUp(Keys.Enter))
                    clicked = true;

                if (_gameState == GameState.PauseMenu &&
                    (clicked && _inGameMenuState.ActiveButton.Text == InGameMenuState.ContinueButtonText
                     || recognized == InGameMenuState.ContinueButtonText ||
                     _lastKeyboardState.IsKeyDown(Keys.Escape) && currentKeyboardState.IsKeyUp(Keys.Escape)))
                {
                    _gameplayState.ResetCountdown();
                    _gameState = GameState.Gameplay;
                }
                else if ((_gameState == GameState.Lose || _gameState == GameState.PauseMenu) &&
                         (clicked && _inGameMenuState.ActiveButton.Text == InGameMenuState.RetryButtonText
                          || recognized == InGameMenuState.RetryButtonText))
                {
                    _gameplayState.Reset();
                    _gameState = GameState.Gameplay;
                }
                else if (clicked && _inGameMenuState.ActiveButton.Text == InGameMenuState.MenuButtonText || recognized == InGameMenuState.MenuButtonText)
                {
                    MediaPlayer.Play(_mainThemeSong);
                    _gameState = GameState.MainMenu;
                }
            }

            _lastKeyboardState = currentKeyboardState;
        }

        protected override void Draw(GameTime gameTime)
        {
            if (!IsActive)
                return;

            GraphicsDevice.Clear(_defaultBackColor);

            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;


            if (_gameState == GameState.Gameplay)
                _gameplayState.Draw(gameTime);

            if (_gameState == GameState.MainMenu)
                _mainMenuState.Draw(gameTime);

            if (_gameState == GameState.Loading)
            {
                _spriteBatch.Begin();
                _spriteBatch.Draw(_mainMenuState.Background, _mainMenuState.BackgroundRec, Color.White);
                DrawText(MainMenuState.LoadingText, new Vector2(Convert.ToInt32(Width / 2), Convert.ToInt32(Height / 2)), 1f, 2f, Color.White, Color.Black, true,
                    TextPosition.Center);
                _spriteBatch.End();
            }

            if (_gameState == GameState.Victory || _gameState == GameState.Lose || _gameState == GameState.PauseMenu)
            {
                _gameplayState.Draw(gameTime);
                _inGameMenuState.Draw(gameTime);
            }

            base.Draw(gameTime);

            if (_userService.IsSaving)
            {
                _spriteBatch.Begin();
                _mainMenuState.SaveText.Draw(_spriteBatch);
                _spriteBatch.End();
            }
        }

        private void ToggleFullScreen()
        {
            if (_graphics.IsFullScreen)
            {
                _graphics.PreferredBackBufferWidth = Convert.ToInt32(ConfigurationManager.AppSettings["Width"]);
                _graphics.PreferredBackBufferHeight = Convert.ToInt32(ConfigurationManager.AppSettings["Height"]);
            }
            else
            {
                _graphics.PreferredBackBufferWidth = GraphicsDevice.Adapter.CurrentDisplayMode.Width;
                _graphics.PreferredBackBufferHeight = GraphicsDevice.Adapter.CurrentDisplayMode.Height;
            }

            Width = _graphics.PreferredBackBufferWidth;
            Height = _graphics.PreferredBackBufferHeight;

            _graphics.ToggleFullScreen();

            _mainMenuState.GameSizeChanged();
            _inGameMenuState.GameSizeChanged();

            _depthStreamRenderer.GameSizeChanged();
            _colorStreamRenderer.GameSizeChanged();
            Logger.Trace("Game screen size changed");
        }

        private Vector2 DrawText(string text, Vector2 pos, float scale, float shadowScale, Color textColor,
           Color outlineColor, bool isBig, TextPosition textPosition)
        {
            Vector2 textSize;

            var gameSizeScale = Height / DefaultStatics.ScreenHeight;

            if (isBig)
                textSize = _bigFont.MeasureString(text);
            else
                textSize = _menuFont.MeasureString(text);

            var origin = Vector2.Zero;

            if (textPosition == TextPosition.LeftCenter)
                origin = new Vector2(0, textSize.Y / 2);

            if (textPosition == TextPosition.Center)
                origin = new Vector2(textSize.X / 2, textSize.Y / 2);

            if (textPosition == TextPosition.Right)
                origin = new Vector2(textSize.X, 0);

            if (textPosition == TextPosition.RightCenter)
                origin = new Vector2(textSize.X, textSize.Y / 2);

            if (isBig)
            {
                _spriteBatch.DrawString(_bigFont, text,
                    pos + new Vector2(shadowScale * gameSizeScale, shadowScale * gameSizeScale), outlineColor * 0.5f, 0,
                    origin, scale * gameSizeScale, SpriteEffects.None, 1);
                _spriteBatch.DrawString(_bigFont, text, pos, textColor, 0, origin, scale * gameSizeScale,
                    SpriteEffects.None, 1);
            }
            else
            {
                _spriteBatch.DrawString(_menuFont, text,
                    pos + new Vector2(shadowScale * gameSizeScale, shadowScale * gameSizeScale), outlineColor * 0.5f, 0,
                    origin, scale * gameSizeScale, SpriteEffects.None, 1);
                _spriteBatch.DrawString(_menuFont, text, pos, textColor, 0, origin, scale * gameSizeScale,
                    SpriteEffects.None, 1);
            }
            return textSize;
        }

        private void graphics_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            e.GraphicsDeviceInformation.PresentationParameters.RenderTargetUsage = RenderTargetUsage.PreserveContents;
        }


        private void LoadGame(MapLevelEntity mapLevel)
        {
            _gameplayState = new GameplayState(this, _skeletonStreamProcessor, mapLevel, DrawText, Width, Height);
            Logger.Trace("GameplayState loaded");
            _isLoading = false;
        }
    }
}