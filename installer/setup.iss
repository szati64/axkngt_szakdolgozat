#include <idp.iss>

[Setup]
AppId={{C627125F-5E49-45E1-9F2B-401CEFBFA997}
AppName=AXKNGT_Szakdoga
AppVersion=1.0
AppPublisher=Attila Szab�
DefaultDirName={pf}\AXKNGT_Szakdoga
DisableProgramGroupPage=yes
OutputBaseFilename=setup
Compression=lzma
SolidCompression=yes


[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "hungarian"; MessagesFile: "compiler:Languages\Hungarian.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"

[Files]
Source: "Content\*"; DestDir: "{app}\Content"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "AXKNGT_Szakdoga.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "AXKNGT_Szakdoga.exe.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "license.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "maps.json"; DestDir: "{app}"; Flags: ignoreversion
Source: "Microsoft.Speech.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "Newtonsoft.Json.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "NLog.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "NLog.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "backg.bmp"; DestDir: "{tmp}"; Flags: dontcopy

[Icons]
Name: "{commonprograms}\AXKNGT_Szakdoga"; Filename: "{app}\AXKNGT_Szakdoga.exe"
Name: "{commondesktop}\AXKNGT_Szakdoga"; Filename: "{app}\AXKNGT_Szakdoga.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\AXKNGT_Szakdoga.exe"; Description: "{cm:LaunchProgram,AXKNGT_Szakdoga}"; Flags: nowait postinstall skipifsilent

[Code]
function Net4FrameworkIsNotInstalled(): Boolean;
begin
  Result := not RegKeyExists(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v4');
end;

function Xna4IsNotInstalled(): Boolean;
begin
  Result := not (RegKeyExists(HKEY_LOCAL_MACHINE, 'SOFTWARE\WOW6432Node\Microsoft\XNA\Framework\v4.0') or 
    RegKeyExists(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\XNA\Framework\v4.0'));
end;

function Kinect18IsNotInstalled(): Boolean;
begin
  Result := not (RegKeyExists(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Kinect') or
  RegKeyExists(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Speech Server\v11.0\Recognizers\Tokens\SR_MS_en-US_Kinect_11.0'));
end;

function GetSystemMetrics(nIndex:Integer):Integer;
external 'GetSystemMetrics@user32.dll stdcall';

procedure InitializeWizard;
var
  width,height: Integer;
  BackgroundBitmapImage: TBitmapImage;
  s: string;
begin
  ExtractTemporaryFile('backg.bmp');
  s:=ExpandConstant('{tmp}')+'\backg.bmp';
  WizardForm.Position:=poScreenCenter;
  MainForm.BORDERSTYLE:=bsNone;
  width:=GetSystemMetrics(0);
  height:=GetSystemMetrics(1);
  MainForm.Width:=width;
  MainForm.Height:=height;
  width:=MainForm.ClientWidth;
  height:=MainForm.ClientHeight;
  MainForm.Left := 0;
  MainForm.Top := 0;

  BackgroundBitmapImage := TBitmapImage.Create(MainForm);
  BackgroundBitmapImage.Bitmap.LoadFromFile(s);
  BackgroundBitmapImage.Align := alClient;
  BackgroundBitmapImage.Parent := MainForm;
  BackgroundBitmapImage.Stretch:=True;
  MainForm.Visible:=True;

  if Net4FrameworkIsNotInstalled() then
  begin
    idpAddFile('https://download.microsoft.com/download/1/B/E/1BE39E79-7E39-46A3-96FF-047F95396215/dotNetFx40_Full_setup.exe', ExpandConstant('{tmp}\Net4FrameworkInstaller.exe'));
    idpDownloadAfter(wpReady);
  end;

  if Xna4IsNotInstalled() then
  begin
    idpAddFile('https://download.microsoft.com/download/5/3/A/53A804C8-EC78-43CD-A0F0-2FB4D45603D3/xnafx40_redist.msi', ExpandConstant('{tmp}\Xna4Installer.msi'));
    idpDownloadAfter(wpReady);
  end;
  
  if Kinect18IsNotInstalled() then
  begin
    idpAddFile('https://download.microsoft.com/download/E/C/5/EC50686B-82F4-4DBF-A922-980183B214E6/KinectRuntime-v1.8-Setup.exe', ExpandConstant('{tmp}\Kinect18Installer.exe'));
    idpDownloadAfter(wpReady);
  end;
end;

procedure InstallFramework;
var
  StatusText: string;
  ResultCode: Integer;
begin
  StatusText := WizardForm.StatusLabel.Caption;
  WizardForm.StatusLabel.Caption := 'Installing Microsoft .NET Framework 4...';
  WizardForm.ProgressGauge.Style := npbstMarquee;
  try
    if not Exec(ExpandConstant('{tmp}\Net4FrameworkInstaller.exe'), '/passive /norestart', '', SW_SHOW, ewWaitUntilTerminated, ResultCode) then
    begin
      MsgBox('.NET installation failed with code: ' + IntToStr(ResultCode) + '.', mbError, MB_OK);
    end;
  finally
    WizardForm.StatusLabel.Caption := StatusText;
    WizardForm.ProgressGauge.Style := npbstNormal;

    DeleteFile(ExpandConstant('{tmp}\Net4FrameworkInstaller.exe'));
  end;
end;

procedure InstallXna;
var
  StatusText: string;
  ResultCode: Integer;
begin
  StatusText := WizardForm.StatusLabel.Caption;
  WizardForm.StatusLabel.Caption := 'Installing Microsoft XNA Framework Redistributable 4.0...';
  WizardForm.ProgressGauge.Style := npbstMarquee;
  try
    if not ShellExec('', 'msiexec', ExpandConstant('/I "{tmp}\Xna4Installer.msi" /passive /norestart'), '', SW_SHOWNORMAL, ewWaitUntilTerminated, ResultCode) then
    begin
      MsgBox('Xna installation failed with code: ' + IntToStr(ResultCode) + '.', mbError, MB_OK);
    end;
  finally
    WizardForm.StatusLabel.Caption := StatusText;
    WizardForm.ProgressGauge.Style := npbstNormal;

    DeleteFile(ExpandConstant('{tmp}\Xna4Installer.msi'));
  end;
end;

procedure InstallKinect;
var
  StatusText: string;
  ResultCode: Integer;
begin
  StatusText := WizardForm.StatusLabel.Caption;
  WizardForm.StatusLabel.Caption := 'Installing Kinect for Windows Runtime v1.8...';
  WizardForm.ProgressGauge.Style := npbstMarquee;
  try
    if not Exec(ExpandConstant('{tmp}\Kinect18Installer.exe'), '', '', SW_SHOW, ewWaitUntilTerminated, ResultCode) then
    begin
      MsgBox('Kinect installation failed with code: ' + IntToStr(ResultCode) + '.', mbError, MB_OK);
    end;
  finally
    WizardForm.StatusLabel.Caption := StatusText;
    WizardForm.ProgressGauge.Style := npbstNormal;

    DeleteFile(ExpandConstant('{tmp}\Kinect18Installer.exe'));
  end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
begin
  case CurStep of
    ssPostInstall:
      begin
          if Net4FrameworkIsNotInstalled() then
            InstallFramework();
          if Xna4IsNotInstalled() then
            InstallXna();
          if Kinect18IsNotInstalled() then
            InstallKinect();
      end;
  end;
end;