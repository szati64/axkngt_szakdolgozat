# AXKNGT_Szakdolgozat

This is the program that I wrote for my thesis. It contains only free to use 3rd party media files, and my code.

## Build

If you want to build this project you have to get these things:

- Visual Studio 2013 or newer
- [XNA 4.0](https://mxa.codeplex.com/)
- Kinect [SDK v1.8 ](https://www.microsoft.com/en-us/download/details.aspx?id=40278) and [Developer Toolkit v1.8](https://www.microsoft.com/en-us/download/details.aspx?id=40276)
- Microsoft Speech Platform 11 [Runtime](https://www.microsoft.com/en-us/download/details.aspx?id=27225) and [SDK](https://www.microsoft.com/en-us/download/details.aspx?id=27226)

`setup.iss` is an [Inno Setup](http://www.jrsoftware.org/isinfo.php) (with [IDP](https://mitrichsoftware.wordpress.com/inno-setup-tools/inno-download-plugin/)) file that can be used to create installer from the compiled files. The installer will check the system for the necessary runtimes when a user tries to install this application. If a runtime is not found on the computer it will download it from the internet and install it.